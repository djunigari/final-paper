#include "../header/ClockThread.h"

ClockThread::ClockThread(){
    i = 1;
    format = "mm:ss:zzz";
    timeCount = new QDateTime();
    timeCount->setMSecsSinceEpoch(totalTime = 0);
    timer = NULL;
    status = INIT;
}
ClockThread::~ClockThread(){
    exit();
    wait();
    delete(timer);
    delete(timeCount);
}
ClockThread* ClockThread::setFormat(QString format){
    this->format = format;
    return this;
}
QString ClockThread::getFormat(){
    return this->format;
}
void ClockThread::run()
{
    if(status == INIT){
        if(timer != NULL){
            disconnect(timer, SIGNAL(timeout()), this, SLOT(timerHit()));
            delete(timer);
        }
        totalTime= 0;
        timer = new QTimer();
        connect(timer, SIGNAL(timeout()), this, SLOT(timerHit()), Qt::DirectConnection);
        timer->start(i);
        //timeClock->show();
    }else if(status == PLAY){
        timer->start(i);
    }
    exec();
    if(status == PAUSE){
        timer->stop();
    }
}
void ClockThread::timerHit()
{
    timeCount->setMSecsSinceEpoch(totalTime+=i);
    emit sendTime(timeCount->toString(format)) ;
}
void ClockThread::pause(){
    status = PAUSE;
    this->exit();
}
void ClockThread::play(){
    status = PLAY;
    start();
}
void ClockThread::reset(){
    status = RESET;
    timeCount->setMSecsSinceEpoch(totalTime= 0);
    emit sendTime(timeCount->toString(format)) ;
}
QString ClockThread::getTime(){
    return timeCount->toString(format);
}
