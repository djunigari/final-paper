#include "../header/SeamCarvingThread.h"

SeamCarvingThread::SeamCarvingThread(){
    typeProcess = CPU;
    originalImage = NULL;
    originalWidth = 0;
    originalHeight = 0;
    qntRetirarLargura = 0;
    qntRetirarAltura = 0;
}
SeamCarvingThread::~SeamCarvingThread(){
    quit();
    wait();
}
void SeamCarvingThread::run(){
    if(typeProcess == CPU){
        result = carvingCPU(originalImage,qntRetirarLargura,qntRetirarAltura,originalWidth,originalHeight);
    }else{
        result = carvingGPU(originalImage,qntRetirarLargura,qntRetirarAltura,originalWidth,originalHeight);
    }
    free(originalImage);
    emit processCompleted();
    exec();
}
SeamCarvingThread* SeamCarvingThread::setOriginalImage(pixelSeamCarving *originalImage){
    this->originalImage = originalImage;
    return this;
}
SeamCarvingThread* SeamCarvingThread::setTypeProcess(TypeProcess typeProcess){
    this->typeProcess = typeProcess;
    return this;
}
SeamCarvingThread* SeamCarvingThread::setQntColunasRetirar(int qntRetirarLargura){
    this->qntRetirarLargura = qntRetirarLargura;
    return this;
}
SeamCarvingThread* SeamCarvingThread::setQntLinhasRetirar(int qntRetirarAltura){
    this->qntRetirarAltura = qntRetirarAltura;
    return this;
}
SeamCarvingThread* SeamCarvingThread::setOriginalWidth(int originalWidth){
    this->originalWidth = originalWidth;
    return this;
}
SeamCarvingThread* SeamCarvingThread::setOriginalHeight(int originalHeight){
    this->originalHeight = originalHeight;
    return this;
}
resultadoSeamCarving &SeamCarvingThread::getResultSeamCarving(){
    return this->result;
}
