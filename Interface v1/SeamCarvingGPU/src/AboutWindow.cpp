#include "../header/AboutWindow.h"


AboutWindow::AboutWindow(QString filePath, QAction *action, QWidget *parent): QWidget(parent){
    this->filePath = filePath;
    this->action = action;
    this->mainLayout = new QVBoxLayout();
    this->textArea = new QTextEdit();
    this->width = 300;
    this->height = 300;
    this->createWindow();
}

void AboutWindow::createWindow(){
    this->resize(width,height);
    this->textArea->setReadOnly(true);
    this->mainLayout->addWidget(textArea);
    this->setLayout(mainLayout);

    QFile *file = new QFile(filePath);
    if(file->open(QFile::ReadOnly|QFile::Text)){
        textArea->setHtml(file->readAll());
        file->close();
    }
    delete(file);
    this->setAttribute(Qt::WA_DeleteOnClose);
}

AboutWindow::~AboutWindow(){
    delete(textArea);
    delete(mainLayout);
    emit sendClose(action);
}

AboutWindow* AboutWindow::setWidth(int w){
    this->width = w;
    this->resize(width,height);
    return this;
}

AboutWindow* AboutWindow::setHeight(int h){
    this->height = h;
    this->resize(width,height);
    return this;
}
