#include "../header/MainWindow.h"
#include "../header/AboutWindow.h"

using namespace std;

struct tabIndex{
    int index;
    ImageWindow *tab;
};

MainWindow::MainWindow(){
    imageUtils = new ImageUtils;
    originalImage = new QPixmap();
    resultImage = new QPixmap();
    markedImage = new QPixmap();
    clockThread = new ClockThread();
    loadingWindow = new LoadingWindow();

    connect(clockThread,SIGNAL(sendTime(QString)),loadingWindow->getTimeLabel(),SLOT(setText(QString)));
    seamCarvingThread = new SeamCarvingThread();
    connect(seamCarvingThread,SIGNAL(processCompleted()),this,SLOT(setResultSeamCarving()));
    tabSelecionado = "Imagem Original";
    createWindow();
}
MainWindow::~MainWindow(){
    delete(this->imageUtils);
    this->clockThread->terminate();
    this->clockThread->wait();
    delete(this->clockThread);
}

void MainWindow::closeAboutWindow(QAction *action){
    action->setEnabled(true);
    action->setChecked(false);
}

void MainWindow::createWindow(){
    this->mainWindow = new QTabWidget();
    this->originalImageTab = new ImageWindow();
    this->resultImageTab = new ImageWindow();
    this->markedImageTab = new ImageWindow();
    //this->console = new Console();

    //this->mainWindow->setTabsClosable(true);
    this->mainWindow->setMovable(true);
    this->mainWindow->addTab(this->originalImageTab,"Imagem Original");
    this->mainWindow->addTab(this->resultImageTab,"Imagem Redimensionada");
    this->mainWindow->addTab(this->markedImageTab,"Seams Retirados");
    connect(mainWindow,SIGNAL(currentChanged(int)),this,SLOT(setTabSelecionado(int)));
    //this->mainWindow->addTab(this->console,"Console");

    this->setCentralWidget(mainWindow);

    this->createActions();
    this->createMenus();
    this->createToolBars();
    this->statusBar();

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->resize(widthInitial,heightInitial);
}

void MainWindow::createActions(){
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Abrir..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Abrir uma imagem"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Salvar"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setToolTip("Salvar "+ tabSelecionado);
    saveAct->setStatusTip("Salvar "+ tabSelecionado +" no disco");
    connect(saveAct, SIGNAL(triggered()), this, SLOT(saveAs()));
/*
    saveAsAct = new QAction(tr("Salvar como..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setToolTip("Salvar "+ tabSelecionado+ "como...");
    saveAsAct->setStatusTip("Salvar "+tabSelecionado+" com outro nome");
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));
 */
    for (int i = 0; i < MaxRecentFiles; ++i) {
        recentFileActs[i] = new QAction(this);
        recentFileActs[i]->setVisible(false);
        connect(recentFileActs[i], SIGNAL(triggered()),
                this, SLOT(openRecentFile()));
    }

    exitAct = new QAction(QIcon(":/images/delete.png"),tr("Sair"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Sair da aplicação"));
    connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

    howToAct= new QAction(tr("&Como usar..."), this);
    howToAct->setCheckable(true);
    howToAct->setStatusTip(tr("Abrir janela Como usar..."));
    connect(howToAct, SIGNAL(triggered()), this, SLOT(howTo()));

    aboutAct = new QAction(tr("&Sobre..."), this);
    aboutAct->setCheckable(true);
    aboutAct->setStatusTip(tr("Abrir janela Sobre"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    //aboutQtAct = new QAction(QIcon(":/images/help-browser.png"),tr("About &Qt"), this);
    //aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    //connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    fileToolAct = new QAction(tr("Arquivo"), this);
    fileToolAct->setCheckable(true);
    fileToolAct->setChecked(true);
    fileToolAct->setStatusTip(tr("Mostrar Ferramentas de Arquivo"));
    connect(fileToolAct, SIGNAL(triggered()), this, SLOT(showFileTool()));

    processToolAct = new QAction(tr("Processamento"), this);
    processToolAct->setCheckable(true);
    processToolAct->setChecked(true);
    processToolAct->setStatusTip(tr("Mostrar Ferramentas de Processamento"));
    connect(processToolAct, SIGNAL(triggered()), this, SLOT(showProcessTool()));

    playAct = new QAction(QIcon(":/images/play.png"),tr("Play"), this);
    playAct->setStatusTip(tr("Rodar processamento SeamCarving"));
    connect(playAct, SIGNAL(triggered()), this, SLOT(play()));

    originalImageAct = new QAction(tr("Imagem Original"), this);
    originalImageAct->setCheckable(true);
    originalImageAct->setChecked(true);
    originalImageAct->setStatusTip(tr("Mostrar Imagem Original"));
    connect(originalImageAct, SIGNAL(triggered()), this, SLOT(showOriginalImageTab()));

    resultImageAct = new QAction(tr("Imagem Redimensionada"), this);
    resultImageAct->setCheckable(true);
    resultImageAct->setChecked(true);
    resultImageAct->setStatusTip(tr("Mostrar Imagem Redimensionada"));
    connect(resultImageAct, SIGNAL(triggered()), this, SLOT(showResultImageTab()));

    markedImageAct = new QAction(tr("Seams Retirados"), this);
    markedImageAct->setCheckable(true);
    markedImageAct->setChecked(true);
    markedImageAct->setStatusTip(tr("Mostrar Seams Retirados"));
    connect(markedImageAct, SIGNAL(triggered()), this, SLOT(showMarkedImageTab()));

    //consoleAct = new QAction(tr("Console"), this);
    //consoleAct->setCheckable(true);
    //consoleAct->setChecked(true);
    //consoleAct->setStatusTip(tr("Mostrar Console"));
    //connect(consoleAct, SIGNAL(triggered()), this, SLOT(showConsole()));

    /*m_sigmapper = new QSignalMapper(this);
    m_sigmapper->setMapping(originalImageAct, );
    connect(mainWindow, SIGNAL(tabCloseRequested(int)),m_sigmapper,SLOT(map(int)));
    connect(m_sigmapper, SIGNAL(mapped(int ,QAction *)),this, SLOT(closeTab(int ,QAction *)));
    */
}
void MainWindow::createMenus(){
    fileMenu = menuBar()->addMenu(tr("Arquivo"));
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    //fileMenu->addAction(saveAsAct);
    separatorAct = fileMenu->addSeparator();
    for (int i = 0; i < MaxRecentFiles; ++i)
        fileMenu->addAction(recentFileActs[i]);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);
    updateRecentFileActions();

    menuBar()->addSeparator();
    windowMenu = menuBar()->addMenu(tr("Ferramentas"));
    windowMenu->addAction(fileToolAct);
    windowMenu->addAction(processToolAct);

    menuBar()->addSeparator();
    viewMenu = menuBar()->addMenu(tr("Janelas"));
    viewMenu->addAction(originalImageAct);
    viewMenu->addAction(resultImageAct);
    viewMenu->addAction(markedImageAct);
    //viewMenu->addAction(consoleAct);

    menuBar()->addSeparator();
    helpMenu = menuBar()->addMenu(tr("&Ajuda"));
    helpMenu->addAction(howToAct);
    helpMenu->addAction(aboutAct);
    //helpMenu->addAction(aboutQtAct);
}
void MainWindow::createToolBars(){
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(saveAct);

    processToolBar = addToolBar(tr("Process"));
    processToolBar->addAction(playAct);

    sWidth = new QSpinBox;
    sHeight = new QSpinBox;
    sWidth->setMinimum(0);
    sHeight->setMinimum(0);
    sWidth->setMinimumWidth(60);
    sHeight->setMinimumWidth(60);
    sWidth->setMaximumWidth(60);
    sHeight->setMaximumWidth(60);

    processToolBar->addWidget(new QLabel(" largura: "));
    processToolBar->addWidget(sWidth);
    processToolBar->addWidget(new QLabel(" Altura: "));
    processToolBar->addWidget(sHeight);
    processToolBar->addWidget(new QLabel(" Tipo de Processamento: "));

    radioCPU = new QRadioButton(tr("CPU"));
    radioGPU = new QRadioButton(tr("GPU"));

    radioCPU->setChecked(true);

    processToolBar->addWidget(radioCPU);
    processToolBar->addWidget(radioGPU);
    gpuQuery gpu = getGpuQuery(2,0);

    if(gpu.existeGpu){
        radioGPU->setStatusTip("Selecionar execução em "+QString(gpu.nomeGpu.c_str()));
    }
    else {
        radioGPU->setStatusTip("Opção desabilitada pois "+QString(gpu.detalhesErro.c_str()));
        radioGPU->setEnabled(false);
    }
    radioCPU->setStatusTip("Selecionar execução em CPU");

    processToolBar->addWidget(new QLabel(" Tempo do processamento : "));
    timeClock = new QLCDNumber();
    timeClock->setDigitCount(clockThread->getFormat().size());
    timeClock->display(clockThread->getTime());
    processToolBar->addWidget(timeClock);
}
void MainWindow::open(){
    QString filterImageFileBrowser = "Todos Arquivos Imagem (*.bmp ; *.dib ; *.jpg ; *.jpeg ; *.jpe ; *.jfif ; *.gif ; *.tif ; *.tiff ; *.png ; *.ico);;Bitmap (*.bmp ; *.dib);;JPEG (*.jpg ; *.jpeg ; *.jpe ; *.jfif);;GIF (*.gif);;TIFF (*.tif ; *.tiff);;PNG (*.png);;ICO (*.ico)";
    QString fileName = QFileDialog::getOpenFileName(this,"Abrir Imagem", QDir::currentPath(),filterImageFileBrowser);
    if (fileName.isEmpty()) return;
    this->openImage(fileName);
}

void MainWindow::openImage(const QString &fileName){
    if (fileName.isEmpty())return;

    this->setCurrentFile(fileName);
    delete(this->originalImage);
    delete(this->resultImage);
    delete(this->markedImage);

    this->originalImage = new QPixmap();
    this->resultImage = new QPixmap();
    this->markedImage = new QPixmap();

    this->originalImage->load(fileName);

    this->updateImage();
    this->mainWindow->setCurrentWidget(this->originalImageTab);
}

void MainWindow::updateImage(){
    originalImageTab->setImage(originalImage)->updateImage();
    resultImageTab->setImage(resultImage)->updateImage();
    markedImageTab->setImage(markedImage)->updateImage();
    setTabSelecionado(0);
}

void MainWindow::save(){
    if (curFile.isEmpty()) saveAs();
    else saveFile(curFile);
}

void MainWindow::saveAs(){
    QString fileName = QFileDialog::getSaveFileName(this,tr("Salvar Imagem"), QDir::currentPath(), tr("Image Files (*.png)"));
    if (fileName.isEmpty()) return;
    saveFile(fileName);
}

void MainWindow::saveFile(const QString &fileName){
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::warning(this, tr("Recent Files"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    QWidget *currentWidget = mainWindow->currentWidget();
    if(currentWidget == resultImageTab){
        resultImage->save(fileName);
    }else if(currentWidget == markedImageTab){
        markedImage->save(fileName);
    }else{
        originalImage->save(fileName);
    }
    QApplication::restoreOverrideCursor();

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("Imagem Salva"), 2000);
}

void MainWindow::openRecentFile(){
    QAction *action = qobject_cast<QAction *>(sender());
    if(action) loadFile(action->data().toString());
}

void MainWindow::howTo(){
    if(!howToAct->isChecked())return;
    howToAct->setChecked(true);
    howToAct->setEnabled(false);
    AboutWindow *howToWindow = (new AboutWindow(":/pages/HelpWindow.html", howToAct))
            ->setWidth(600)
            ->setHeight(600);
    connect(howToWindow, SIGNAL(sendClose(QAction*)),this,SLOT(closeAboutWindow(QAction*)));
    howToWindow->show();
}
void MainWindow::about(){
    if(!aboutAct->isChecked())return;
    aboutAct->setChecked(true);
    aboutAct->setEnabled(false);
    AboutWindow *aboutWindow = (new AboutWindow(":/pages/AboutWindow.html", aboutAct))
            ->setWidth(500)
            ->setHeight(325);
    connect(aboutWindow, SIGNAL(sendClose(QAction*)),this,SLOT(closeAboutWindow(QAction*)));
    aboutWindow->show();
}

void MainWindow::loadFile(const QString &fileName){
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, tr("Arquivos Recentes"),
                             tr("Não foi possível abrir o arquivo %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    QApplication::restoreOverrideCursor();
    this->openImage(fileName);
    statusBar()->showMessage(tr("Arquivo Carregado"), 2000);
}

void MainWindow::setCurrentFile(const QString &fileName){
    curFile = fileName;
    QFileInfo *finfo = new QFileInfo(fileName);
    setWindowFilePath(curFile);
    setWindowTitle(finfo->fileName()+" - SeamCarving");
    QSettings settings;
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(fileName);
    files.prepend(fileName);
    while (files.size() > MaxRecentFiles)
        files.removeLast();

    settings.setValue("recentFileList", files);

    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
        MainWindow *mainWin = qobject_cast<MainWindow *>(widget);
        if (mainWin)
            mainWin->updateRecentFileActions();
    }
}

void MainWindow::updateRecentFileActions(){
    QSettings settings;
    QStringList files = settings.value("recentFileList").toStringList();

    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i) {
        QString text = tr("&%1 %2").arg(i + 1).arg(strippedName(files[i]));
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFileActs[j]->setVisible(false);

    separatorAct->setVisible(numRecentFiles > 0);
}

QString MainWindow::strippedName(const QString &fullFileName){
    return QFileInfo(fullFileName).fileName();
}

void MainWindow::showFileTool(){
    fileToolBar->setVisible(fileToolAct->isChecked());
    fileToolAct->setChecked(fileToolAct->isChecked());
}

void MainWindow::showProcessTool(){
    processToolBar->setVisible(processToolAct->isChecked());
    processToolAct->setChecked(processToolAct->isChecked());
}
void MainWindow::showOriginalImageTab(){
    if(originalImageAct->isChecked())
        this->mainWindow->addTab(this->originalImageTab,"Imagem Original");
    else
        this->mainWindow->removeTab(this->mainWindow->indexOf(this->originalImageTab));
    originalImageAct->setChecked(originalImageAct->isChecked());
}
void MainWindow::showResultImageTab(){
    if(resultImageAct->isChecked())
        this->mainWindow->addTab(this->resultImageTab,"Imagem Redimensionada");
    else
        this->mainWindow->removeTab(this->mainWindow->indexOf(this->resultImageTab));
    resultImageAct->setChecked(resultImageAct->isChecked());
}
void MainWindow::showMarkedImageTab(){
    if(markedImageAct->isChecked())
        this->mainWindow->addTab(this->markedImageTab,"Seams Retirados");
    else
        this->mainWindow->removeTab(this->mainWindow->indexOf(this->markedImageTab));
    markedImageAct->setChecked(markedImageAct->isChecked());
}
void MainWindow::showConsole(){
    /*if(consoleAct->isChecked())
        this->mainWindow->addTab(this->console,"Console");
    else
        this->mainWindow->removeTab(this->mainWindow->indexOf(this->console));
    consoleAct->setChecked(consoleAct->isChecked());
    */
}
void MainWindow::closeTab(int i,QAction *action){
    action->setChecked(false);
    this->mainWindow->removeTab(i);
}
void MainWindow::play(){
    setVisible(false);
    //console->clean();
    loadingWindow->createWindow();

    pixelSeamCarving *originalImagePixel = imageUtils->convertQPixmapToPixelSeamCarving(*this->originalImage);
    int originalWidth = this->originalImage->width();
    int originalHeight = this->originalImage->height();

    int resultWidth = sWidth->text().toInt();
    int resultHeight = sHeight->text().toInt();

    int qntRetirarLargura = originalWidth - resultWidth;
    int qntRetirarAltura = originalHeight - resultHeight;

    TypeProcess typeProcess = CPU;
    if(!radioCPU->isChecked())typeProcess=GPU;
    seamCarvingThread->setOriginalImage(originalImagePixel)
    ->setTypeProcess(typeProcess)
    ->setQntColunasRetirar(qntRetirarLargura)
    ->setQntLinhasRetirar(qntRetirarAltura)
    ->setOriginalWidth(originalWidth)
    ->setOriginalHeight(originalHeight);
    clockThread->start();
    seamCarvingThread->start();
}


void MainWindow::setResultSeamCarving(){
    resultadoSeamCarving result = seamCarvingThread->getResultSeamCarving();

    if (result.imagemFinal == NULL) {
        clockThread->quit();
        clockThread->wait();
        timeClock->display(clockThread->getTime());
        seamCarvingThread->quit();
        seamCarvingThread->wait();
        loadingWindow->close();
        QMessageBox::warning(this, tr("ERRO :("),"Ocorreu um erro na execução SeamCarving.\nPor favor tente novamente com uma imagem menor.");
    }
    else {
        int originalWidth = this->originalImage->width();
        int originalHeight = this->originalImage->height();

        int resultWidth = sWidth->text().toInt();
        int resultHeight = sHeight->text().toInt();

        if(resultImage != NULL)delete(resultImage);
        if(markedImage != NULL)delete(markedImage);

        resultImage = imageUtils->convertPixelSeamCarvingToQPixmap(result.imagemFinal,resultWidth,resultHeight);
        markedImage = imageUtils->convertPixelRGBToQPixmap(result.imagemMarcada,originalWidth,originalHeight);

        updateImage();
        mainWindow->setCurrentWidget(resultImageTab);
        free(result.imagemFinal);
        free(result.imagemMarcada);

        clockThread->quit();
        clockThread->wait();
        timeClock->display(clockThread->getTime());
        seamCarvingThread->quit();
        seamCarvingThread->wait();
        loadingWindow->close();
    }
    setVisible(true);
}
void MainWindow::setTabSelecionado(int i){    
    QPixmap *image = originalImageTab->getImage();
    tabSelecionado = "Imagem Original";

    QWidget *currentWidget = mainWindow->currentWidget();
    if(currentWidget == resultImageTab){
        image = resultImageTab->getImage();
        tabSelecionado = "Imagem Redimensionada";
    }else if(currentWidget == markedImageTab){
        image = markedImageTab->getImage();
        tabSelecionado = "Seams Retirados";
    }
    saveAct->setStatusTip("Salvar "+ tabSelecionado +" no disco");
    saveAct->setToolTip("Salvar "+ tabSelecionado);
    int w = i;
    w = 0;
    int h = 0;
    if(image != NULL){
        w = image->width();
        h = image->height();
    }
    sWidth->setMaximum(w);
    sHeight->setMaximum(h);
    sWidth->setValue(w);
    sHeight->setValue(h);
}
