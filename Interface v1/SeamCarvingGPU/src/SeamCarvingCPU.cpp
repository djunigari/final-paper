﻿/* SeamCarvinCPU.cpp
 * Responsável por executar o algoritmo na CPU.
 *
 * Author: ar1
 *
 */
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <omp.h>
#include "../header/SeamCarving.h"

using namespace std;

inline char melhorDos3_CPU(float a, float b, float c) {
    if(a < b && a < c)
        return -1;
    if( b <= c )
        return 0;

    return 1;
}

void calculaEnergia_CPU(pixelSeamCarving* imagem, pixelRGB* imagemMarcada, int largura, int altura) {
    int offset;
    pixelRGB pixelAtual;
    for(int i=0; i < altura; i++) {
        //#pragma omp parallel for private(offset)
        for(int j = 0; j< largura; j++) {
            offset = i * largura + j;
            pixelAtual = imagem[offset].pixel;

            imagem[offset].energia = 0.2126f * pixelAtual.red  +
                                    0.7152f * pixelAtual.green +
                                    0.0722f * pixelAtual.blue;
//Y = 0.2126 R + 0.7152 G + 0.0722 B
            imagemMarcada[offset] = pixelAtual;
        }
    }
}

void calculaGradient_CPU (pixelSeamCarving *imagem, int largura, int altura) {
    int offset;

    float gradientX, gradientY, pixelAtual, pixelEsq, pixelDir, pixelAcima, pixelAbaixo,
        pixelCanto1, pixelCanto2, pixelCanto3, pixelCanto4;
    int x[] = {1, 0, -1,
               2, 0, -2,
               1, 0, -1 };

    int y[] = {1,  2,  1,
               0,  0,  0,
              -1, -2, -1 };

    offset = 0; //ponto superior esquerdo
//início
        pixelAtual = imagem[offset].energia;
        pixelDir = imagem[offset+1].energia; //direita
        pixelCanto4 = imagem[offset+largura+1].energia; //direita inferior
        pixelAbaixo = imagem[offset+largura].energia; //abaixo

            gradientX =   x[0] *  pixelAtual + //replicado
                          x[1] *  pixelAtual + //replicado
                          x[2] *  pixelDir + //replicado
                          x[3] *  pixelAtual + //replicado
                          x[4] *  pixelAtual +
                          x[5] *  pixelDir +
                          x[6] *  pixelAbaixo + //replicado
                          x[7] *  pixelAbaixo +
                          x[8] *  pixelCanto4;

            gradientY =   y[0] *  pixelAtual + //replicado
                          y[1] *  pixelAtual + //replicado
                          y[2] *  pixelDir + //replicado
                          y[3] *  pixelAtual + //replicado
                          y[4] *  pixelAtual +
                          y[5] *  pixelDir +
                          y[6] *  pixelAbaixo + //replicado
                          y[7] *  pixelAbaixo +
                          y[8] *  pixelCanto4;

     imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);
//fim ponto superior esquerdo

    offset = largura-1; //ponto superior direito
        pixelAtual = imagem[offset].energia;
        pixelEsq = imagem[offset-1].energia; //esquerda
        pixelCanto3 = imagem[offset+largura-1].energia; //esquerda inferior
        pixelAbaixo = imagem[offset+largura].energia; //abaixo

            gradientX = 	  x[0] *  pixelEsq + //replicado
                              x[1] *  pixelAtual + //replicado
                              x[2] *  pixelAtual + //replicado
                              x[3] *  pixelEsq +
                              x[4] *  pixelAtual +
                              x[5] *  pixelAtual + //replicado
                              x[6] *  pixelCanto3 +
                              x[7] *  pixelAbaixo +
                              x[8] *  pixelAbaixo; //replicado

            gradientY = 	  y[0] *  pixelEsq + //replicado
                              y[1] *  pixelAtual + //replicado
                              y[2] *  pixelAtual + //replicado
                              y[3] *  pixelEsq +
                              y[4] *  pixelAtual +
                              y[5] *  pixelAtual + //replicado
                              y[6] *  pixelCanto3 +
                              y[7] *  pixelAbaixo +
                              y[8] *  pixelAbaixo; //replicado
     imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);
//fim ponto superior direito

    offset = (largura * altura - 1);  //ponto inferior direito (último pixel)
            pixelAtual = imagem[offset].energia;
            pixelEsq = imagem[offset-1].energia; //esquerda
            pixelCanto1 = imagem[offset-largura-1].energia; //esquerda superior
            pixelAcima = imagem[offset-largura].energia; //acima

            gradientX =  x[0] *  pixelCanto1 +
                         x[1] *  pixelAcima +
                         x[2] *  pixelAcima + //replicado
                         x[3] *  pixelEsq +
                         x[4] *  pixelAtual +
                         x[5] *  pixelAtual +//replicado
                         x[6] *  pixelEsq +//replicado
                         x[7] *  pixelAtual +//replicado
                         x[8] *  pixelAtual;//replicado

            gradientY =  y[0] *  pixelCanto1 +
                         y[1] *  pixelAcima +
                         y[2] *  pixelAcima + //replicado
                         y[3] *  pixelEsq +
                         y[4] *  pixelAtual +
                         y[5] *  pixelAtual +//replicado
                         y[6] *  pixelEsq +//replicado
                         y[7] *  pixelAtual +//replicado
                         y[8] *  pixelAtual;//replicado
                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);
//fim ponto inferior direito (último pixel)

        offset = ((altura-1) * largura); //ponto inferior esquerdo
            pixelAtual = imagem[offset].energia;
            pixelDir = imagem[offset+1].energia; //direita
            pixelCanto2 = imagem[offset-largura+1].energia; //direita superior
            pixelAcima = imagem[offset-largura].energia; //acima

            gradientX = 	 x[0] *  pixelAcima +//replicado
                             x[1] *  pixelAcima +
                             x[2] *  pixelCanto2 +
                             x[3] *  pixelAtual + //replicado
                             x[4] *  pixelAtual +
                             x[5] *  pixelDir +
                             x[6] *  pixelAtual + //replicado
                             x[7] *  pixelAtual + //replicado
                             x[8] *  pixelDir; //replicado

            gradientY = 	 y[0] *  pixelAcima +//replicado
                             y[1] *  pixelAcima +
                             y[2] *  pixelCanto2 +
                             y[3] *  pixelAtual + //replicado
                             y[4] *  pixelAtual +
                             y[5] *  pixelDir +
                             y[6] *  pixelAtual + //replicado
                             y[7] *  pixelAtual + //replicado
                             y[8] *  pixelDir; //replicado

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);
//fim ponto inferior esquerdo

    offset = largura;

    for(int i = 1; i< altura-1; i++) {
         //primeira coluna

        pixelAtual = imagem[offset].energia;
        pixelAcima = imagem[offset-largura].energia;
        pixelAbaixo = imagem[offset+largura].energia;
        pixelCanto2 = imagem[offset-largura +1].energia; //acima direita
        pixelDir = imagem[offset+1].energia; //direita
        pixelCanto4 = imagem[offset+largura+1].energia; //direita inferior


         //primeira coluna
        gradientX = x[0] *  pixelAcima + //replicado
                    x[1] *  pixelAcima +
                    x[2] *  pixelCanto2 +
                    x[3] *  pixelAtual + //replicado
                    x[4] *  pixelAtual +
                    x[5] *  pixelDir +
                    x[6] *  pixelAbaixo + //replicado
                    x[7] *  pixelAbaixo +
                    x[8] *  pixelCanto4;

        gradientY = y[0] *  pixelAcima + //replicado
                    y[1] *  pixelAcima +
                    y[2] *  pixelCanto2 +
                    y[3] *  pixelAtual + //replicado
                    y[4] *  pixelAtual +
                    y[5] *  pixelDir +
                    y[6] *  pixelAbaixo + //replicado
                    y[7] *  pixelAbaixo +
                    y[8] *  pixelCanto4;

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);

        offset += largura-1;
         //última coluna

        pixelAtual = imagem[offset].energia;
        pixelAcima = imagem[offset-largura].energia;
        pixelAbaixo = imagem[offset+largura].energia;
        pixelCanto1 = imagem[offset-largura-1].energia; //acima esquerda
        pixelEsq = imagem[offset-1].energia; //esquerda
        pixelCanto3 = imagem[offset+largura-1].energia; //esquerda inferior

         gradientX =  x[0] *  pixelCanto1 +
                      x[1] *  pixelAcima +
                      x[2] *  pixelAcima + //replicado
                      x[3] *  pixelEsq +
                      x[4] *  pixelAtual +
                      x[5] *  pixelAtual + 		//replicado
                      x[6] *  pixelCanto3 +
                      x[7] *  pixelAbaixo +
                      x[8] *  pixelAbaixo;	//replicado

         gradientY =  y[0] *  pixelCanto1 +
                      y[1] *  pixelAcima +
                      y[2] *  pixelAcima + //replicado
                      y[3] *  pixelEsq +
                      y[4] *  pixelAtual +
                      y[5] *  pixelAtual + 		//replicado
                      y[6] *  pixelCanto3 +
                      y[7] *  pixelAbaixo +
                      y[8] *  pixelAbaixo;	//replicado

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);

        offset++;

    }
//fim primeira e última coluna

for(int i = 1; i < largura-1; i++) {
    offset = i;
    //primeira linha
        pixelAtual = imagem[offset].energia;
        pixelEsq = imagem[offset-1].energia;
        pixelDir = imagem[offset+1].energia;
        pixelCanto3 = imagem[offset+largura-1].energia; //esquerda inferior
        pixelAbaixo = imagem[offset+largura].energia; //abaixo
        pixelCanto4 = imagem[offset+largura+1].energia; //direita inferior

        gradientX =	x[0] *  pixelEsq + //replicado
                    x[1] *  pixelAtual +  //replicado
                    x[2] *  pixelDir + //replicado
                    x[3] *  pixelEsq +
                    x[4] *  pixelAtual +
                    x[5] *  pixelDir +
                    x[6] *  pixelCanto3 +
                    x[7] *  pixelAbaixo +
                    x[8] *  pixelCanto4;

        gradientY =	y[0] *  pixelEsq + //replicado
                    y[1] *  pixelAtual +  //replicado
                    y[2] *  pixelDir + //replicado
                    y[3] *  pixelEsq +
                    y[4] *  pixelAtual +
                    y[5] *  pixelDir +
                    y[6] *  pixelCanto3 +
                    y[7] *  pixelAbaixo +
                    y[8] *  pixelCanto4;

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);
    offset = ((altura-1) * largura)	+i;
        //última linha
        pixelAtual = imagem[offset].energia;
        pixelEsq = imagem[offset-1].energia;
        pixelDir = imagem[offset+1].energia;
        pixelCanto1 = imagem[offset-largura-1].energia; //esquerda superior
        pixelAcima = imagem[offset-largura].energia; //acima
        pixelCanto2 = imagem[offset-largura+1].energia; //direita superior

        gradientX =	x[0] *  pixelCanto1 +
                    x[1] *  pixelAcima +
                    x[2] *  pixelCanto2 +
                    x[3] *  pixelEsq +
                    x[4] *  pixelAtual +
                    x[5] *  pixelDir +
                    x[6] *  pixelEsq + //replicado
                    x[7] *  pixelAtual + //replicado
                    x[8] *  pixelDir; //replicado

        gradientY =	y[0] *  pixelCanto1 +
                    y[1] *  pixelAcima +
                    y[2] *  pixelCanto2 +
                    y[3] *  pixelEsq +
                    y[4] *  pixelAtual +
                    y[5] *  pixelDir +
                    y[6] *  pixelEsq + //replicado
                    y[7] *  pixelAtual + //replicado
                    y[8] *  pixelDir; //replicado

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);

}
//fim primeira e última linha

    for(int i=1; i < altura-1; i++) {
        //#pragma omp parallel for private(offset)
        for(int j = 1; j< largura-1; j++) {
            offset = i * largura + j;
            //todos os pixels que não estão na borda da imagem
            pixelCanto1 =  imagem[offset-largura-1].energia;
            pixelAcima =   imagem[offset-largura].energia;
            pixelCanto2 =  imagem[offset-largura+1].energia;
            pixelEsq =     imagem[offset-1].energia;
            pixelAtual =   imagem[offset].energia;
            pixelDir =     imagem[offset+1].energia;
            pixelCanto3 =  imagem[offset+largura-1].energia;
            pixelAbaixo =  imagem[offset+largura].energia;
            pixelCanto4 =  imagem[offset+largura+1].energia;

            //todos os pixels que não estão na borda da imagem
            gradientX = x[0] *  pixelCanto1 +
                        x[1] *  pixelAcima  +
                        x[2] *  pixelCanto2 +
                        x[3] *  pixelEsq 	+
                        x[4] *  pixelAtual 	+
                        x[5] *  pixelDir 	+
                        x[6] *  pixelCanto3 +
                        x[7] *  pixelAbaixo +
                        x[8] *  pixelCanto4;


            gradientY = y[0] *  pixelCanto1 +
                        y[1] *  pixelAcima  +
                        y[2] *  pixelCanto2 +
                        y[3] *  pixelEsq 	+
                        y[4] *  pixelAtual 	+
                        y[5] *  pixelDir 	+
                        y[6] *  pixelCanto3 +
                        y[7] *  pixelAbaixo +
                        y[8] *  pixelCanto4;

                 imagem[offset].custo = sqrt( gradientX *  gradientX +  gradientY *  gradientY);

        }
    }
}

void calculaCusto_CPU(pixelSeamCarving* imagem, int largura, int altura) {
    int offset, auxOffset, colunaAtual;
    char deslocamento;

    for(int i=1; i < altura; i++) {
        //#pragma omp parallel for private(offset, auxOffset)
        for(int j = 0; j < largura; j++) {
            offset = i * largura + j;
            colunaAtual = offset % largura;
            auxOffset = offset - largura; //pixel na linha de cima

            if(colunaAtual == 0) { //primeira coluna
                deslocamento =
                    (imagem[auxOffset].custo <= imagem[auxOffset+1].custo) ? 0: 1;
            }
            else if(colunaAtual == largura-1) { //última coluna
                deslocamento =
                    (imagem[auxOffset-1].custo < imagem[auxOffset].custo) ? -1 : 0;

            }
            else {
                deslocamento = melhorDos3_CPU( imagem[auxOffset-1].custo,
                                 imagem[auxOffset].custo,
                                 imagem[auxOffset+1].custo);

            }
            auxOffset += deslocamento;
            imagem[offset].custo +=  imagem[auxOffset].custo;
            imagem[offset].deslocamento = deslocamento;
        }
    }
}

void calculaCaminho_CPU(pixelSeamCarving* imagem, pixelRGB* imagemMarcada, int largura, int altura, int* posicaoMarcado) {
    int indiceElementoUltimaLinha = ((altura-1) * largura);
    int pontoInicial = indiceElementoUltimaLinha;
    float menorCusto = imagem[indiceElementoUltimaLinha].custo;


    for(int i = 1; i < largura; i++) { //busca na última linha o menorCusto
        indiceElementoUltimaLinha = ((altura-1) * largura) + i;
        if(imagem[indiceElementoUltimaLinha].custo < menorCusto) {
            menorCusto = imagem[indiceElementoUltimaLinha].custo;
            pontoInicial = indiceElementoUltimaLinha;

        }
    }


    for(int linhaAtual = altura-1; linhaAtual >= 0; linhaAtual--) { //percorre a imagem para marcar o seam

        pixelSeamCarving pixelAtual = imagem[pontoInicial];

        posicaoMarcado[linhaAtual] = pontoInicial;

        pixelAtual.pixel.red = 200;
        pixelAtual.pixel.green = 0;
        pixelAtual.pixel.blue = 0;
        imagemMarcada[pixelAtual.originalOffset] = pixelAtual.pixel;

        pontoInicial = pontoInicial - largura + pixelAtual.deslocamento; //move para o melhor ponto dos três de cima do pixel atual
    }

}

void retiraPontos_CPU(pixelSeamCarving* imagem, pixelSeamCarving* imagemAux,int* posicaoMarcado, int largura, int altura) {
        pixelSeamCarving pixelAtual;
        int novaLargura = largura -1;
        int linhaAtual;
        int colunaAtual;
        int novoOffset;
        int offset;
        int offsetPixelRetirado;

    for(int i = 0; i < altura; i++)  {
        //#pragma omp parallel for private(offset)
        for(int j = 0; j < largura; j++) {
            offset = i * largura + j;
            pixelAtual = imagem[offset];
            linhaAtual = offset / largura;
            colunaAtual = offset % largura;
            novoOffset = linhaAtual * novaLargura + colunaAtual;
            offsetPixelRetirado = posicaoMarcado[linhaAtual];

            if(offset > offsetPixelRetirado) {
                novoOffset--; //sua posição será uma a menos que na imagem original
                imagemAux[novoOffset] = pixelAtual;
            }
            else if(offset < offsetPixelRetirado) {
                imagemAux[novoOffset] = pixelAtual; //sem modificações no pixel
            }//se o offset for a posição marcada, não faz nada
        }
    }
}

void inverteImagem_CPU(pixelSeamCarving* imagem, pixelSeamCarving* imagemAux, int largura, int altura) {
    int offset, novoOffset;

    //#pragma omp parallel for
    for(int x = 0; x < largura; x++) {
        //#pragma omp parallel for private(offset, novoOffset)
        for(int y = 0; y < altura; y++) {
            offset = y * largura + x;
            novoOffset = x * altura + y;

            pixelSeamCarving pixelAtual = imagem[offset];

            imagemAux[novoOffset] = pixelAtual;
        }
    }


}

void liberaMemoria_CPU(pixelSeamCarving* imagemAux, int* posicaoMarcado) {
    free(imagemAux);
    free(posicaoMarcado);
}


resultadoSeamCarving carvingCPU(pixelSeamCarving* imagem, int qntRetirarLargura, int qntRetirarAltura, int largura, int altura) {


    pixelSeamCarving* imagemAux;
    pixelSeamCarving* imagemFinal;
    pixelRGB* imagemMarcada;
    pixelSeamCarving* tmp;
    int* posicaoMarcado;
    resultadoSeamCarving resultado;

    int qntDePixels = largura * altura;
    int maiorDimensao = largura >= altura ? largura : altura;


    imagemAux = (pixelSeamCarving*) malloc(qntDePixels * sizeof(pixelSeamCarving));
    imagemFinal = (pixelSeamCarving*) malloc(qntDePixels * sizeof(pixelSeamCarving));
    imagemMarcada = (pixelRGB*) malloc(qntDePixels * sizeof(pixelRGB));
    posicaoMarcado = (int*) malloc(maiorDimensao * sizeof(int));

    if(imagemAux == NULL
     || imagemFinal == NULL
     || imagemMarcada == NULL
     || posicaoMarcado == NULL ) {
        cout << "Falha ao alocar memoria\n"<< endl;

        liberaMemoria_CPU(imagemAux, posicaoMarcado);
        free(imagemFinal);
        free(imagemMarcada);
        resultado.imagemFinal = NULL;
        resultado.imagemMarcada = NULL;
        return resultado;
     }

    //copia a imagem recebida para a imagem final e para a imagem marcada
    memcpy(imagemFinal, imagem, sizeof(pixelSeamCarving)*qntDePixels);

    //calcula energia de cada pixel e já copia a imagem para a imagem marcada
    calculaEnergia_CPU(imagemFinal, imagemMarcada, largura, altura);

    if(qntRetirarLargura > 0) {
        for(int i = 0; i < qntRetirarLargura; i++ ) {

            //calcular gradient; filtro de sobel e salva o custo em cada pixel
            calculaGradient_CPU(imagemFinal, largura, altura);

            //custo de cada caminho
            calculaCusto_CPU(imagemFinal, largura, altura);

            calculaCaminho_CPU(imagemFinal, imagemMarcada, largura, altura, posicaoMarcado);

            //retirar os pixels marcados (transfere para imagemAux)
            retiraPontos_CPU(imagemFinal, imagemAux, posicaoMarcado, largura, altura);

            //Troca imagem por imagemAux
            tmp = imagemFinal;
            imagemFinal = imagemAux;
            imagemAux = tmp;

            //atualiza a nova largura
            largura--;

        }
        //cout << "Término de retirada de colunas"<< endl;
    }

    if(qntRetirarAltura > 0) {

        inverteImagem_CPU(imagemFinal, imagemAux, largura, altura);
        tmp = imagemFinal;
        imagemFinal = imagemAux;
        imagemAux = tmp;

        int tmpLargura = largura;
        largura = altura;
        altura = tmpLargura;


        //cout << "Fim da rotação1"<< endl;

        for(int i = 0; i < qntRetirarAltura; i++ ) {


            calculaGradient_CPU(imagemFinal, largura, altura);


            //custo de cada caminho
            calculaCusto_CPU(imagemFinal, largura, altura);

            calculaCaminho_CPU(imagemFinal, imagemMarcada, largura, altura, posicaoMarcado);


            //retirar os pixels marcados (transfere para imagemAux)
            retiraPontos_CPU(imagemFinal, imagemAux, posicaoMarcado, largura, altura);
            //Troca imagem por imagemAux
            tmp = imagemFinal;
            imagemFinal = imagemAux;
            imagemAux = tmp;

            //atualiza a nova largura
            largura--;
        }
        //cout << "Término de retirada de linhas"<< endl;
        inverteImagem_CPU(imagemFinal, imagemAux, largura, altura);
        tmp = imagemFinal;
        imagemFinal = imagemAux;
        imagemAux = tmp;

        tmpLargura = largura;
        largura = altura;
        altura = tmpLargura;

        //cout << "Fim da rotação2"<< endl;

    }

    liberaMemoria_CPU(imagemAux, posicaoMarcado);

    resultado.imagemFinal = imagemFinal;
    resultado.imagemMarcada = imagemMarcada;
    return resultado;
}
