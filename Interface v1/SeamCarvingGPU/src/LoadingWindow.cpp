#include "../header/LoadingWindow.h"

LoadingWindow::LoadingWindow(QWidget *parent): QWidget(parent){
    mainLayout =  new QVBoxLayout();
    msg = new QLabel("A execução poderá levar alguns minutos\nPor favor aguarde o processamento SeamCarving terminar");
    time = new QLabel();
    msg->setAlignment(Qt::AlignCenter);
    time->setAlignment(Qt::AlignCenter);
    movie = new QMovie(":/GIF/ajax-loader.gif");

    image = new QLabel();
    image->setMovie(movie);
    image->setMinimumWidth(msg->width());
    image->setAlignment(Qt::AlignCenter);

    mainLayout->addWidget(msg);
    mainLayout->addWidget(image);
    mainLayout->addWidget(time);
    setLayout(mainLayout);
    setWindowFlags(Qt::WindowTitleHint);
}
LoadingWindow::~LoadingWindow(){
    delete(movie);
    delete(image);
    delete(msg);
    delete(mainLayout);
}

void LoadingWindow::createWindow(){
    show();
    movie->start();
}

QLabel* LoadingWindow::getTimeLabel(){
    return this->time;
}
