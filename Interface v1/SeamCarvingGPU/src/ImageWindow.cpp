#include "../header/ImageWindow.h"
#include <QLabel>

ImageWindow::ImageWindow(QWidget *parent): QWidget(parent){
    this->mainLayout = new QVBoxLayout();
    this->bottomLayout = new QHBoxLayout();
    this->bottomWindow = new QWidget();
    this->scene = new QGraphicsScene();
    this->graphicsView = new QGraphicsView();
    this->pixmapItem = new QGraphicsPixmapItem();
    this->fitButton = new QToolButton();
    this->zoomSlider = new QSlider(Qt::Horizontal);
    this->image = NULL;
    this->createWindow();
}

ImageWindow::~ImageWindow(){
    delete(this->pixmapItem);
    delete(this->scene);
    delete(this->graphicsView);
    delete(this->fitButton);
    delete(this->zoomSlider);
    delete(this->bottomLayout);
    delete(this->bottomWindow);
    delete(this->mainLayout);

    this->mainLayout = NULL;
    this->bottomLayout = NULL;
    this->bottomWindow = NULL;
    this->scene = NULL;
    this->graphicsView = NULL;
    this->pixmapItem = NULL;
    this->fitButton = NULL;
    this->zoomSlider = NULL;
}

ImageWindow* ImageWindow::setImage(QPixmap *image){
    this->image = image;
    return this;
}

QPixmap *ImageWindow::getImage(){
    return image;
}

void ImageWindow::createWindow(){
    connect(fitButton, SIGNAL(clicked()), this, SLOT(fitInOutView()));
    connect(zoomSlider, SIGNAL(sliderMoved(int)),this, SLOT(zoom(int)));

    zoomSlider->setFocusPolicy(Qt::StrongFocus);
    zoomSlider->setTickPosition(QSlider::TicksBothSides);
    zoomSlider->setTickInterval(25);
    zoomSlider->setMinimum(0);
    zoomSlider->setMaximum(100);
    this->fitIn = true;
    this->m_scale = 1;
    this->fitInOutView();
    this->bottomLayout->addWidget(this->zoomSlider);
    this->bottomLayout->addWidget(this->fitButton);
    this->bottomWindow->setLayout(this->bottomLayout);

    this->scene->addItem(this->pixmapItem);
    this->graphicsView->setScene(this->scene);
    this->mainLayout->addWidget(this->graphicsView);
    this->mainLayout->addWidget(this->bottomWindow);

    this->setLayout(mainLayout);
}

void ImageWindow::fitInOutView(){
    QIcon icon;
    double newScale = 1;
    icon = QIcon(":/images/fitIn.png");
    if(!(this->fitIn  = !this->fitIn)){
        if(this->image != NULL){
            if(this->image->width() > this->graphicsView->width() && this->image->height() > this->graphicsView->height()){
                icon = QIcon(":/images/fitOut.png");
                double aux1 = this->graphicsView->width()/(this->image->width()*1.0);
                double aux2 = this->graphicsView->height()/(this->image->height()*1.0);
                if(aux1 > aux2) newScale = aux2;
                else newScale = aux1;
            }else if(this->image->width() > this->graphicsView->width()){
                icon = QIcon(":/images/fitOut.png");
                newScale = this->graphicsView->width()/(this->image->width()*1.0);
            }else if(this->image->height() > this->graphicsView->height()){
                icon = QIcon(":/images/fitOut.png");
                newScale = this->graphicsView->height()/(this->image->height()*1.0);
            }
        }
    }

    this->fitButton->setIcon(icon);
    double scale = this->calcScale(newScale);
    this->setScale(newScale);
    zoomSlider->setValue(this->m_scale*100);
    this->graphicsView->scale(scale,scale);
}

//arrumar
void ImageWindow::updateImage(){
    if(image == NULL)return;
    pixmapItem->setPixmap(*image);
    scene->setSceneRect(image->rect());
    fitIn = true;
    fitInOutView();
}

void ImageWindow::zoom(int i){
    if(i <= 0 || i > 100) return;
    double aux  = i / 100.0;
    double scale = this->calcScale(aux);
    this->setScale(aux);
    this->graphicsView->scale(scale,scale);
    zoomSlider->setToolTip(QString::number(i)+"%");
    QToolTip::showText(QCursor::pos(),QString::number(i) + QString("%"));
}

void ImageWindow::setScale(double scale){
    this->m_scale = scale;
}

double ImageWindow::calcScale(double scale){
    return scale/m_scale;
}
