#include "../header/Console.h"

Console::Console(QWidget *parent):QWidget(parent){
    this->textArea = new QTextEdit();
    this->mainLayout = new QVBoxLayout();

    new Q_DebugStream(cout, this->textArea); //Redirect Console output to QTextEdit
    Q_DebugStream::registerQDebugMessageHandler(); //Redirect qDebug() output to QTextEdit

    this->createWindow();
}
void Console::clean(){
    this->textArea->clear();
}
Console::~Console(){
    delete(this->textArea);
    delete(this->mainLayout);

    this->textArea = NULL;
    this->mainLayout = NULL;
}

void Console::createWindow(){
    this->mainLayout->addWidget(this->textArea);
    this->setLayout(this->mainLayout);
}

void Console::println(char *msg){
    QString *message = new QString(msg);
    this->textArea->append(*message);
    delete(message);
}

QTextEdit* Console::getTextArea(){
    return this->textArea;
}
