/*
 * VerificaGPU.cu
 *
 * Responsável por verificar se existem GPUs disponíveis	
 *      
 *
 * Author: ar1
 *
 */

#include "../header/VerificaGPU.h"

extern "C++" {

	
	gpuQuery getGpuQuery(int minimumCapability_major, int minimumCapability_minor) {
	
		gpuQuery detalhesGPU;
	    int deviceCount = 0;
		cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

		if (error_id != cudaSuccess)
		{
			detalhesGPU.existeGpu = false;
			detalhesGPU.majorCapability = 0;
			detalhesGPU.minorCapability = 0;
			detalhesGPU.nomeGpu = ""; 
            detalhesGPU.detalhesErro = "Não foi possível encontrar GPU CUDA neste sistema.";
			return detalhesGPU;
		}

		
		if (deviceCount == 0)
		{
			detalhesGPU.existeGpu = false;
			detalhesGPU.majorCapability = 0;
			detalhesGPU.minorCapability = 0;
			detalhesGPU.nomeGpu = "";
			detalhesGPU.detalhesErro = "Não encontrado GPU compativel com CUDA neste sistema";
			return detalhesGPU;
		}		
		
		int dev;	
		for (dev = 0; dev < deviceCount; ++dev)
		{
		    cudaSetDevice(dev);
		    cudaDeviceProp deviceProp;
		    cudaGetDeviceProperties(&deviceProp, dev);
		   	detalhesGPU.majorCapability = deviceProp.major;
			detalhesGPU.minorCapability = deviceProp.minor;
			detalhesGPU.nomeGpu = deviceProp.name;
			
			if(detalhesGPU.majorCapability < minimumCapability_major) {				
				detalhesGPU.existeGpu = false;	
		    	detalhesGPU.detalhesErro = "Requisitos de capacidade de processamento não são atendidos pela GPU";
			}
			else if(detalhesGPU.majorCapability == minimumCapability_major 
					&& detalhesGPU.minorCapability < minimumCapability_minor ) {
				detalhesGPU.existeGpu = false;	
		    	detalhesGPU.detalhesErro = "Requisitos de capacidade de processamento não são atendidos pela GPU";
			}
			else {
				detalhesGPU.existeGpu = true;
				detalhesGPU.detalhesErro = "";
			}

		}
		
		return detalhesGPU;
	
	
	
	}
}

