#include "../header/ImageUtils.h"

pixelSeamCarving* ImageUtils::convertQPixmapToPixelSeamCarving(const QPixmap &image){

    pixelSeamCarving *pixels = (pixelSeamCarving*) malloc(sizeof(pixelSeamCarving)*image.width()*image.height());

    QImage image_aux = image.toImage();
    for (int x = 0; x<image_aux.width(); x++){
        for (int y = 0; y<image_aux.height(); y++){
            int i = y * image.width() + x;
            pixelSeamCarving p;
            QRgb rgb = image_aux.pixel(x,y);
            p.pixel.red = qRed(rgb);
            p.pixel.green = qGreen(rgb);
            p.pixel.blue = qBlue(rgb);
            p.originalOffset = i;
            pixels[i] = p;
        }
    }
    return pixels;
}

QPixmap* ImageUtils::convertPixelSeamCarvingToQPixmap(const pixelSeamCarving *pixels, const int &width,const int &heigth){
    QImage *image = new QImage(width,heigth,QImage::Format_RGB32);

    for(int x = 0; x < width; x++) {
        for(int y = 0; y < heigth; y++) {
            int i = y * width + x;
            QRgb rgb = qRgb(pixels[i].pixel.red,pixels[i].pixel.green,pixels[i].pixel.blue);
            image->setPixel(x,y,rgb);
        }
    }
    QPixmap *result = new QPixmap(QPixmap::fromImage(*image));
    delete(image);
    image = NULL;
    return result;
}

QPixmap* ImageUtils::convertPixelRGBToQPixmap(const pixelRGB *pixels, const int &width, const int &heigth){
    QImage *image = new QImage(width,heigth,QImage::Format_RGB32);

    for(int x = 0; x < width; x++) {
        for(int y = 0; y < heigth; y++) {
            int i = y * width + x;
            QRgb rgb = qRgb(pixels[i].red,pixels[i].green,pixels[i].blue);
            image->setPixel(x,y,rgb);
        }
    }
    QPixmap *result = new QPixmap(QPixmap::fromImage(*image));
    delete(image);
    image = NULL;
    return result;
}
