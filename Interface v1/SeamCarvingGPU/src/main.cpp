#include <QApplication>
#include "../header/MainWindow.h"
#include "../header/AboutWindow.h"
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName("TCC_Project");
    app.setApplicationName("SeamCarving");
    app.setWindowIcon(QIcon(":images/app.ico"));
    MainWindow *mainWin = new MainWindow;
    mainWin->moveToThread(QApplication::instance()->thread());
    mainWin->show();
    return app.exec();
}
