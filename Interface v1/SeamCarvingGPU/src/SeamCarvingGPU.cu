/* SeamCarvinGPU.cu
 * Responsável por executar o algoritmo na GPU.
 *
 * Author: ar1
 *
 */

#include "../header/SeamCarving.h"
#include <iostream>
#include <math.h>

#define THREADS_PER_BLOCK 1024


__global__ void calculaEnergia(pixelSeamCarving* d_imagem, pixelRGB* d_imagemMarcada, int largura, int altura) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;	

	if(tid < largura *altura) {
		pixelSeamCarving pixelAtual = d_imagem[tid];
/*		pixelAtual.energia = 0.3f * pixelAtual.pixel.red  +
								0.59f * pixelAtual.pixel.green +
								0.11f * pixelAtual.pixel.blue;
Y = 0.2126 R + 0.7152 G + 0.0722 B.
*/
		pixelAtual.energia = 	0.2126f * pixelAtual.pixel.red  +
								0.7152f * pixelAtual.pixel.green +
								0.0722f * pixelAtual.pixel.blue;
		pixelAtual.custo = 0;
		d_imagem[tid] = pixelAtual;
		d_imagemMarcada[tid] = pixelAtual.pixel;
	}
}



__global__ void calculaGradient_cantos(pixelSeamCarving* d_imagem, int largura, int altura) {
	//quatro threads (cantos da imagem)
	int tid = threadIdx.x;
	float gradientX, gradientY, vizinhoA, vizinhoB, vizinhoC, energiaPixelAtual;
	
	char x[] = {1, 0, -1,
			   2, 0, -2,
			   1, 0, -1 };

	char y[] = {1,  2,  1,
			   0,  0,  0,
			  -1, -2, -1 };
	
	if(tid == 0) {		
	 	//ponto superior esquerdo
		energiaPixelAtual = d_imagem[tid].energia;
		vizinhoA = d_imagem[tid+1].energia; //direita
		vizinhoB = d_imagem[tid+largura+1].energia; //direita inferior
		vizinhoC = d_imagem[tid+largura].energia; //abaixo
		
			gradientX =   x[0] *  energiaPixelAtual + //replicado
						  x[1] *  energiaPixelAtual + //replicado
						  x[2] *  vizinhoA + //replicado
						  x[3] *  energiaPixelAtual + //replicado
						  x[4] *  energiaPixelAtual +
						  x[5] *  vizinhoA +
						  x[6] *  vizinhoC + //replicado
						  x[7] *  vizinhoC +
						  x[8] *  vizinhoB;
	
			gradientY =   y[0] *  energiaPixelAtual + //replicado
						  y[1] *  energiaPixelAtual + //replicado
						  y[2] *  vizinhoA + //replicado
						  y[3] *  energiaPixelAtual + //replicado
						  y[4] *  energiaPixelAtual +
						  y[5] *  vizinhoA +
						  y[6] *  vizinhoC + //replicado
						  y[7] *  vizinhoC +
						  y[8] *  vizinhoB;
	}
	  else if(tid == 1) {
	  	//ponto superior direito
	  	tid = largura-1;
		energiaPixelAtual = d_imagem[tid].energia;
		vizinhoA = d_imagem[tid-1].energia; //esquerda
		vizinhoB = d_imagem[tid+largura-1].energia; //esquerda inferior
		vizinhoC = d_imagem[tid+largura].energia; //abaixo
				
			gradientX = 	  x[0] *  vizinhoA + //replicado
							  x[1] *  energiaPixelAtual + //replicado
							  x[2] *  energiaPixelAtual + //replicado
							  x[3] *  vizinhoA +
							  x[4] *  energiaPixelAtual +
							  x[5] *  energiaPixelAtual + //replicado
							  x[6] *  vizinhoB +
							  x[7] *  vizinhoC +
							  x[8] *  vizinhoC; //replicado

			gradientY = 	  y[0] *  vizinhoA + //replicado
							  y[1] *  energiaPixelAtual + //replicado
							  y[2] *  energiaPixelAtual + //replicado
							  y[3] *  vizinhoA +
							  y[4] *  energiaPixelAtual +
							  y[5] *  energiaPixelAtual + //replicado
							  y[6] *  vizinhoB +
							  y[7] *  vizinhoC +
							  y[8] *  vizinhoC; //replicado
		}
		else if(tid == 2) {
		//ponto inferior esquerdo
			tid = (altura-1) * largura;

			energiaPixelAtual = d_imagem[tid].energia;
			vizinhoA = d_imagem[tid+1].energia; //direita
			vizinhoB = d_imagem[tid-largura+1].energia; //direita superior
			vizinhoC = d_imagem[tid-largura].energia; //acima
			
			gradientX = 	 x[0] *  vizinhoC +//replicado
							 x[1] *  vizinhoC +
							 x[2] *  vizinhoB +
							 x[3] *  energiaPixelAtual + //replicado
							 x[4] *  energiaPixelAtual +
							 x[5] *  vizinhoA +
							 x[6] *  energiaPixelAtual + //replicado
							 x[7] *  energiaPixelAtual + //replicado
							 x[8] *  vizinhoA; //replicado

			gradientY = 	 y[0] *  vizinhoC +//replicado
							 y[1] *  vizinhoC +
							 y[2] *  vizinhoB +
							 y[3] *  energiaPixelAtual + //replicado
							 y[4] *  energiaPixelAtual +
							 y[5] *  vizinhoA +
							 y[6] *  energiaPixelAtual + //replicado
							 y[7] *  energiaPixelAtual + //replicado
							 y[8] *  vizinhoA; //replicado
		}
		else  { 
		//ponto inferior direito (último pixel)
			tid = largura * altura - 1;
			energiaPixelAtual = d_imagem[tid].energia;
			vizinhoA = d_imagem[tid-1].energia; //esquerda
			vizinhoB = d_imagem[tid-largura-1].energia; //esquerda superior
			vizinhoC = d_imagem[tid-largura].energia; //acima
			
			gradientX =  x[0] *  vizinhoB +
						 x[1] *  vizinhoC +
						 x[2] *  vizinhoC + //replicado
						 x[3] *  vizinhoA +
						 x[4] *  energiaPixelAtual +
						 x[5] *  energiaPixelAtual +//replicado
						 x[6] *  vizinhoA +//replicado
						 x[7] *  energiaPixelAtual +//replicado
						 x[8] *  energiaPixelAtual;//replicado

			gradientY =  y[0] *  vizinhoB +
						 y[1] *  vizinhoC +
						 y[2] *  vizinhoC + //replicado
						 y[3] *  vizinhoA +
						 y[4] *  energiaPixelAtual +
						 y[5] *  energiaPixelAtual +//replicado
						 y[6] *  vizinhoA +//replicado
						 y[7] *  energiaPixelAtual +//replicado
						 y[8] *  energiaPixelAtual;//replicado
		}
		
		d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);
}


__global__ void calculaGradient_vert(pixelSeamCarving* d_imagem, int largura, int altura) {
	//uma thread para cada linha
	int linhaAtual = blockIdx.x * blockDim.x + threadIdx.x;
	if(linhaAtual < (altura-1)
		&& linhaAtual > 0) {
		
		int tid = linhaAtual * largura; //primeira coluna
		float gradientX, gradientY, pixelAtual, pixelAcima, pixelAbaixo, pixelLateral1, pixelLateral2, pixelLateral3;
		
		char x[] = {1, 0, -1,
				   2, 0, -2,
				   1, 0, -1 };
	
		char y[] = {1,  2,  1,
				   0,  0,  0,
				  -1, -2, -1 };
		
		pixelAtual = d_imagem[tid].energia;
		pixelAcima = d_imagem[tid-largura].energia;
		pixelAbaixo = d_imagem[tid+largura].energia;
		pixelLateral1 = d_imagem[tid-largura +1].energia; //acima direita
		pixelLateral2 = d_imagem[tid+1].energia; //direita
		pixelLateral3 = d_imagem[tid+largura+1].energia; //direita inferior
		
		
		 //primeira coluna
		gradientX = x[0] *  pixelAcima + //replicado
					x[1] *  pixelAcima +
					x[2] *  pixelLateral1 +
					x[3] *  pixelAtual + //replicado
					x[4] *  pixelAtual +
					x[5] *  pixelLateral2 +
					x[6] *  pixelAbaixo + //replicado
					x[7] *  pixelAbaixo +
					x[8] *  pixelLateral3;

		gradientY = y[0] *  pixelAcima + //replicado
					y[1] *  pixelAcima +
					y[2] *  pixelLateral1 +
					y[3] *  pixelAtual + //replicado
					y[4] *  pixelAtual +
					y[5] *  pixelLateral2 +
					y[6] *  pixelAbaixo + //replicado
					y[7] *  pixelAbaixo +
					y[8] *  pixelLateral3;
					
		d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);
		
		//última coluna
		tid += largura-1;
		
		pixelAtual = d_imagem[tid].energia;
		pixelAcima = d_imagem[tid-largura].energia;
		pixelAbaixo = d_imagem[tid+largura].energia;
		pixelLateral1 = d_imagem[tid-largura-1].energia; //acima esquerda
		pixelLateral2 = d_imagem[tid-1].energia; //esquerda
		pixelLateral3 = d_imagem[tid+largura-1].energia; //esquerda inferior
		
		 gradientX =  x[0] *  pixelLateral1 +
					  x[1] *  pixelAcima +
					  x[2] *  pixelAcima + //replicado
					  x[3] *  pixelLateral2 +
					  x[4] *  pixelAtual +
					  x[5] *  pixelAtual + 		//replicado
					  x[6] *  pixelLateral3 +
					  x[7] *  pixelAbaixo +
					  x[8] *  pixelAbaixo;	//replicado

		 gradientY =  y[0] *  pixelLateral1 +
					  y[1] *  pixelAcima +
					  y[2] *  pixelAcima + //replicado
					  y[3] *  pixelLateral2 +
					  y[4] *  pixelAtual +
					  y[5] *  pixelAtual + 		//replicado
					  y[6] *  pixelLateral3 +
					  y[7] *  pixelAbaixo +
					  y[8] *  pixelAbaixo;	//replicado
		
		d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);
	}
}



__global__ void calculaGradient_hor(pixelSeamCarving* d_imagem, int largura, int altura) {
	//uma thread para cada coluna
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid < (largura-1)
		&& tid > 0) {
		
		float gradientX, gradientY, pixelAtual, pixelEsq, pixelDir, pixelLateral1, pixelLateral2, pixelLateral3;
		char x[] = {1, 0, -1,
				   2, 0, -2,
				   1, 0, -1 };
	
		char y[] = {1,  2,  1,
				   0,  0,  0,
				  -1, -2, -1 };

		//primeira linha
		pixelAtual = d_imagem[tid].energia;
		pixelEsq = d_imagem[tid-1].energia;
		pixelDir = d_imagem[tid+1].energia;
		pixelLateral1 = d_imagem[tid+largura-1].energia; //esquerda inferior
		pixelLateral2 = d_imagem[tid+largura].energia; //abaixo
		pixelLateral3 = d_imagem[tid+largura+1].energia; //direita inferior
		
		gradientX =	x[0] *  pixelEsq + //replicado
					x[1] *  pixelAtual +  //replicado
					x[2] *  pixelDir + //replicado
					x[3] *  pixelEsq +
					x[4] *  pixelAtual +
					x[5] *  pixelDir +
					x[6] *  pixelLateral1 +
					x[7] *  pixelLateral2 +
					x[8] *  pixelLateral3;

		gradientY =	y[0] *  pixelEsq + //replicado
					y[1] *  pixelAtual +  //replicado
					y[2] *  pixelDir + //replicado
					y[3] *  pixelEsq +
					y[4] *  pixelAtual +
					y[5] *  pixelDir +
					y[6] *  pixelLateral1 +
					y[7] *  pixelLateral2 +
					y[8] *  pixelLateral3;
		
		d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);
		
		//última linha		 
		tid += ((altura-1) * largura);
		pixelAtual = d_imagem[tid].energia;
		pixelEsq = d_imagem[tid-1].energia;
		pixelDir = d_imagem[tid+1].energia;
		pixelLateral1 = d_imagem[tid-largura-1].energia; //esquerda superior
		pixelLateral2 = d_imagem[tid-largura].energia; //acima
		pixelLateral3 = d_imagem[tid-largura+1].energia; //direita superior
		
		gradientX =	x[0] *  pixelLateral1 + 
					x[1] *  pixelLateral2 +  
					x[2] *  pixelLateral3 + 
					x[3] *  pixelEsq +
					x[4] *  pixelAtual +
					x[5] *  pixelDir +
					x[6] *  pixelEsq + //replicado
					x[7] *  pixelAtual + //replicado
					x[8] *  pixelDir; //replicado

		gradientY =	y[0] *  pixelLateral1 + 
					y[1] *  pixelLateral2 +  
					y[2] *  pixelLateral3 + 
					y[3] *  pixelEsq +
					y[4] *  pixelAtual +
					y[5] *  pixelDir +
					y[6] *  pixelEsq + //replicado
					y[7] *  pixelAtual + //replicado
					y[8] *  pixelDir; //replicado
										
		d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);						
	}
}

__global__ void calculaGradient(pixelSeamCarving* d_imagem, int largura, int altura) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= largura
		&& tid < largura * (altura-1)) { //não processa os pixels da primeira e última linha
				
		int colunaAtual = tid % largura;
		if(colunaAtual != 0
			&& colunaAtual != largura-1) { //não processa os pixels da primeira e última coluna
			
			float gradientX, gradientY, pixelAtual, pixelEsq, pixelDir, pixelAcima, pixelAbaixo,
				pixelCanto1, pixelCanto2, pixelCanto3,
				pixelCanto4;
			
			char x[] = {1, 0, -1,
					   2, 0, -2,
					   1, 0, -1 };
	
			char y[] = {1,  2,  1,
					   0,  0,  0,
					  -1, -2, -1 };
			
			pixelCanto1 =  d_imagem[tid-largura-1].energia;
			pixelAcima =   d_imagem[tid-largura].energia;
			pixelCanto2 =  d_imagem[tid-largura+1].energia;
			pixelEsq =     d_imagem[tid-1].energia;
			pixelAtual =   d_imagem[tid].energia;
			pixelDir =     d_imagem[tid+1].energia;
			pixelCanto3 =  d_imagem[tid+largura-1].energia;
			pixelAbaixo =  d_imagem[tid+largura].energia;
			pixelCanto4 =  d_imagem[tid+largura+1].energia;
			
			//todos os pixels que não estão na borda da d_imagem
			gradientX = x[0] *  pixelCanto1 +
						x[1] *  pixelAcima  +
						x[2] *  pixelCanto2 +
						x[3] *  pixelEsq 	+
						x[4] *  pixelAtual 	+
						x[5] *  pixelDir 	+
						x[6] *  pixelCanto3 +
						x[7] *  pixelAbaixo +
						x[8] *  pixelCanto4;


			gradientY = y[0] *  pixelCanto1 +
						y[1] *  pixelAcima  +
						y[2] *  pixelCanto2 +
						y[3] *  pixelEsq 	+
						y[4] *  pixelAtual 	+
						y[5] *  pixelDir 	+
						y[6] *  pixelCanto3 +
						y[7] *  pixelAbaixo +
						y[8] *  pixelCanto4;
											
			d_imagem[tid].custo = sqrt(gradientX * gradientX + gradientY * gradientY);		
		}	
	}
}

__device__ inline char melhorDos3(float a, float b, float c) {
	if(a < b && a < c)
		return -1;
	if( b <= c )
		return 0;

	return 1;
}

__global__ void calculaCusto(pixelSeamCarving* d_imagem, int largura, int altura, int linhaAtual) {
	int colunaAtual = blockIdx.x * blockDim.x + threadIdx.x;
	if(colunaAtual < largura) {
		int offset;
		int auxOffset;
		char deslocamento;

		offset = linhaAtual * largura + colunaAtual;
		auxOffset = offset - largura; //pixel na linha de cima
			
		pixelSeamCarving pixelAtual = d_imagem[offset]; //cria uma variável local pois o acesso é mais rápido
		
		if(colunaAtual == 0) { //primeira coluna
			deslocamento = 
				(d_imagem[auxOffset].custo <= d_imagem[auxOffset+1].custo) ? 0: 1;
		}
		else if(colunaAtual == largura-1) { //última coluna
			deslocamento = 
				(d_imagem[auxOffset-1].custo < d_imagem[auxOffset].custo) ? -1 : 0;
		}
		else {
			deslocamento = melhorDos3(d_imagem[auxOffset-1].custo,
							d_imagem[auxOffset].custo,
							d_imagem[auxOffset+1].custo);

		}
		pixelAtual.custo += d_imagem[auxOffset + deslocamento].custo; //Acumula o próprio custo com o menor dos seus vizinhos de cima
		pixelAtual.deslocamento = deslocamento; //já salva o melhor dos vizinhos, pra não precisar calcular de novo

		d_imagem[offset] = pixelAtual;
	}
}

__global__ void calculaCaminho(pixelSeamCarving* d_imagem, pixelRGB* d_imagemMarcada, int* d_posicaoMarcado, int largura, int altura) {
	//somente uma thread executa
	int indiceElementoUltimaLinha = ((altura-1) * largura);
	int posicaoPixelMarcado = indiceElementoUltimaLinha;
	pixelSeamCarving pixelAtual;
	float menorCusto = d_imagem[indiceElementoUltimaLinha].custo;


	for(int i = 1; i < largura; i++) { //busca na última linha o menorCusto
		indiceElementoUltimaLinha = ((altura-1) * largura) + i;
		if(d_imagem[indiceElementoUltimaLinha].custo < menorCusto) {
			menorCusto = d_imagem[indiceElementoUltimaLinha].custo;
			posicaoPixelMarcado = indiceElementoUltimaLinha;

		}
	}


	for(int linhaAtual = altura-1; linhaAtual >= 0; linhaAtual--) { //percorre a imagem para marcar o seam
		
		pixelAtual = d_imagem[posicaoPixelMarcado];
		d_posicaoMarcado[linhaAtual] = posicaoPixelMarcado;

		pixelAtual.pixel.red = 200;
		pixelAtual.pixel.green = 0;
		pixelAtual.pixel.blue = 0;
		d_imagemMarcada[pixelAtual.originalOffset] = pixelAtual.pixel;

		posicaoPixelMarcado = posicaoPixelMarcado - largura + pixelAtual.deslocamento; //move para o melhor ponto dos três de cima do pixel atual
	}
}

__global__ void retiraPontos(pixelSeamCarving* d_imagem, pixelSeamCarving* d_imagemAux, int* d_posicaoMarcado, int largura, int altura) {
	int offset = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(offset < largura * altura) {
		pixelSeamCarving pixelAtual = d_imagem[offset];
		int novaLargura = largura -1;
		int linhaAtual = offset / largura;
		int colunaAtual = offset % largura;
		int novoOffset = linhaAtual * novaLargura + colunaAtual;
		int offsetPixelRetirado = d_posicaoMarcado[linhaAtual];
		
		if(offset > offsetPixelRetirado) {
			novoOffset--; //sua posição será uma a menos que na imagem original
			d_imagemAux[novoOffset] = pixelAtual; //sem modificações no pixel
		}
		else if (offset < offsetPixelRetirado) {
			d_imagemAux[novoOffset] = pixelAtual; //sem modificações no pixel
		} //se o offset for a posição marcada, não faz nada
		
	}
}

__global__ void inverteImagem(pixelSeamCarving* d_imagem, pixelSeamCarving* d_imagemAux, int largura, int altura) {
    int offset = blockIdx.x * blockDim.x + threadIdx.x;
    if(offset < largura * altura) {
        pixelSeamCarving pixelAtual = d_imagem[offset];
		int linhaAtual = offset / largura;
		int colunaAtual = offset % largura;
        int novoOffset = colunaAtual * altura + linhaAtual;

        d_imagemAux[novoOffset] = pixelAtual;
    }

}


//kernel de teste que salva na "imagem marcada" o mapa de energia da imagem
//comentada pois esse método não é utilizado no projeto original
__global__ void realcaBordas(pixelSeamCarving* d_imagem, pixelRGB* d_imagemMarcada, int largura, int altura) {
    int offset = blockIdx.x * blockDim.x + threadIdx.x;
    if(offset < largura * altura) {
        pixelSeamCarving pixelAtual = d_imagem[offset];
        
        unsigned char custo = 255 * pixelAtual.custo /1300;
        
		pixelAtual.pixel.red = custo;
		pixelAtual.pixel.green = custo;
		pixelAtual.pixel.blue = custo;

        d_imagemMarcada[pixelAtual.originalOffset] = pixelAtual.pixel;
    }

}


extern "C++" {

	using namespace std;


	void liberaMemoria(pixelSeamCarving* d_imagem, pixelSeamCarving* d_imagemAux, int* d_posicaoMarcado, pixelRGB* d_imagemMarcada) {

		cudaFree(d_imagem);
		cudaFree(d_imagemAux);
		cudaFree(d_posicaoMarcado);
		cudaFree(d_imagemMarcada);

	}

	/**
	*	Redimensiona em GPU
	*
	*
	*
	*
	*/
	pixelSeamCarving* redimensionaEmGPU(int *d_posicaoMarcado, pixelSeamCarving* d_imagem, pixelRGB* d_imagemMarcada,
						int largura, int altura, int qntRetirar) {
		
		pixelSeamCarving* tmp;
		pixelSeamCarving* d_imagemAux;
		float tempoExecucao = 0;
		float tempoGradient = 0;
		float tempoCusto = 0;
		float tempoMarcarCaminho = 0;
		float tempoRetirar = 0;
		cudaEvent_t start, stop;
		cudaEventCreate( &start );
		cudaEventCreate( &stop );
		cudaError_t erro = cudaSuccess;
		int qntDePixels = largura * altura;
		size_t blocksPerGrid = (qntDePixels + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
		size_t blocksPerRow = (largura + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
		size_t blocksPerColumn = (altura + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
		
		cudaStream_t stream[4];
			for (int i = 0; i < 4; ++i)
				cudaStreamCreate(&stream[i]);
		
		
		//aloca uma imagem auxiliar:
		erro = cudaMalloc((void**)&d_imagemAux, qntDePixels * sizeof(pixelSeamCarving));
			if(erro != cudaSuccess) {
                //cout << "Cuda error imagemAux -- " << cudaGetErrorString(erro) << endl;
                cout << "Falha ao alocar memoria na GPU\n"<< endl;
				liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}



		
		//cudaEventRecord( start, 0);
		//repete até retirar a qnt especificada:
        for(int i = 0; i < qntRetirar; i++ ) {
			cudaEventRecord( start, 0);	
			
			//kernel de calcular gradient cantos
			calculaGradient_cantos<<<1, 4, 0, stream[0]>>>(d_imagem, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error8 -- %s\n" << cudaGetErrorString(erro) << endl;
                liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}
			
			//kernel de calcular gradient primeira e última linha
			calculaGradient_hor<<<blocksPerRow, THREADS_PER_BLOCK, 0, stream[1]>>>(d_imagem, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error8 -- %s\n" << cudaGetErrorString(erro)<< endl;
                liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}
			
			//kernel de calcular gradient primeira e última coluna
			calculaGradient_vert<<<blocksPerColumn, THREADS_PER_BLOCK, 0, stream[2]>>>(d_imagem, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error8 -- %s\n" << cudaGetErrorString(erro) << endl;
                liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}
			
			//kernel de calcular gradient
			calculaGradient<<<blocksPerGrid, THREADS_PER_BLOCK, 0, stream[3]>>>(d_imagem, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error8 -- %s\n" << cudaGetErrorString(erro) << endl;
                liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}

			cudaEventRecord( stop, 0);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
			tempoGradient += tempoExecucao;

//realcaBordas<<<blocksPerGrid, THREADS_PER_BLOCK>>>(d_imagem, d_imagemMarcada, largura, altura);

			cudaEventRecord( start, 0);	
			
			
			for(int linha = 1; linha < altura; linha++) { //começamos na segunda linha
				//kernel de calcular o custo de cada caminho
				//uma thread para cada coluna
				calculaCusto<<<blocksPerRow, THREADS_PER_BLOCK>>>(d_imagem, largura, altura, linha);
				erro = cudaGetLastError();
				if(erro != cudaSuccess) {
                    //cout << "\nCuda error10 -- " << cudaGetErrorString(erro) << endl;
                    liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
					return NULL;
				}				
			}
			
			cudaEventRecord( stop, 0);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
			tempoCusto += tempoExecucao;

			cudaEventRecord( start, 0);	
	
			
			//kernel de marcar o melhor caminho
			//SOMENTE UMA THREAD			
			calculaCaminho<<<1,1>>>(d_imagem, d_imagemMarcada, d_posicaoMarcado, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error11 -- %s\n" << cudaGetErrorString(erro) << endl;
				liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}

			cudaEventRecord( stop, 0);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
			tempoMarcarCaminho += tempoExecucao;
			cudaEventRecord( start, 0);	
	
			
			//kernel para retirar os pixels marcados (transfere os pixels válidos para imagemAux)
			retiraPontos<<<blocksPerGrid, THREADS_PER_BLOCK>>>(d_imagem, d_imagemAux, d_posicaoMarcado, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error12 -- %s\n" << cudaGetErrorString(erro) << endl;
				liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada);
				return NULL;
			}
	
			cudaEventRecord( stop, 0);
			cudaEventSynchronize(stop);
			cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
			tempoRetirar += tempoExecucao;
			
	
	
			//atualiza a nova largura e blocksPerGrid
			largura--;
			qntDePixels = largura * altura;
			blocksPerGrid = (qntDePixels + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
			blocksPerRow = (largura + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
			
			//Troca d_imagem por d_imagemAux (nde estão os pixels válidos)
			tmp = d_imagem;
			d_imagem = d_imagemAux;
			d_imagemAux = tmp;

		}
		
			
		
        //cout << "\nTempo de gradient: " << tempoGradient/1000 <<"s"<< endl;
		
        //cout << "\nTempo de custo: " << tempoCusto/1000<<"s"<< endl;

        //cout << "\nTempo de marcar: " << tempoMarcarCaminho/1000 <<"s"<< endl;

        //cout << "\nTempo de retirar: " << tempoRetirar/1000 <<"s"<< endl;

		
		for(int i = 0; i< 4; i++)
			cudaStreamDestroy(stream[i]);
		cudaFree(d_imagemAux);
							
		return d_imagem;				
	}

	/**
	*	Carving GPU
	*
	*
	*
	**/
    resultadoSeamCarving carvingGPU(pixelSeamCarving* imagem, int qntRetirarLargura, int qntRetirarAltura,  int largura, int altura) {
				
		int   *d_posicaoMarcado;
		int    maiorDimensao = largura >= altura ? largura : altura;
		pixelSeamCarving* d_imagem;
		pixelRGB* d_imagemMarcada;
		pixelSeamCarving* d_imagemAux;
		pixelSeamCarving* tmp;
		resultadoSeamCarving resultado;
		
		int qntDePixels = largura * altura;
		size_t blocksPerGrid = (qntDePixels + (THREADS_PER_BLOCK -1))/THREADS_PER_BLOCK;
		cudaError_t erro = cudaSuccess;

		cudaEvent_t start, stop;
		cudaEventCreate( &start );
		cudaEventCreate( &stop );
		float tempoExecucao = 0;
		cudaEventRecord( start, 0);	
		
		//aloca a imagem onde será salvo o resultado
        resultado.imagemFinal = (pixelSeamCarving*) malloc(sizeof(pixelSeamCarving)*(largura-qntRetirarLargura)*(altura-qntRetirarAltura));
        resultado.imagemMarcada = (pixelRGB*) malloc(qntDePixels * sizeof(pixelRGB));
		if(resultado.imagemFinal == NULL 
			|| resultado.imagemMarcada == NULL) {
            cout << "Falha ao alocar memoria\n"<< endl;
            free(resultado.imagemFinal);
			free(resultado.imagemMarcada);
			resultado.imagemFinal = NULL;
			resultado.imagemMarcada = NULL;
			return resultado;

		}		
		
		erro = cudaMalloc((void**)&d_imagem, qntDePixels * sizeof(pixelSeamCarving));
			if(erro != cudaSuccess) {
                cout << "Falha ao alocar memoria na GPU\n"<< endl;
                liberaMemoria(d_imagem, NULL, NULL, NULL );
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
			}
			
			
		erro = cudaMalloc((void**)&d_imagemMarcada, qntDePixels * sizeof(pixelRGB));
			if(erro != cudaSuccess) {
                //cout << "\nCuda error5 -- "<< cudaGetErrorString(erro)<< endl;
                cout << "Falha ao alocar memoria na GPU\n"<< endl;
                liberaMemoria(d_imagem, NULL, NULL, d_imagemMarcada );
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
			}
			
        erro = cudaMalloc((void**)&d_posicaoMarcado, maiorDimensao * sizeof(int));
			if(erro != cudaSuccess) {
                //cout << "\nCuda error? -- %s\n" <<cudaGetErrorString(erro) << endl;
                cout << "Falha ao alocar memoria na GPU\n"<< endl;
                liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
			}	
						
		cudaEventRecord( stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
        //cout << "\nTempo de alocação: "<< tempoExecucao/1000 <<"s"<< endl;
		

						
		//cópias
		cudaEventRecord( start, 0);			
		//copia para a GPU o array de pixels
		erro = cudaMemcpy(d_imagem, imagem, sizeof(pixelSeamCarving)*qntDePixels,cudaMemcpyHostToDevice);
			if(erro != cudaSuccess) {
                //cout << "\nCuda error6 -- %s\n"<< cudaGetErrorString(erro) << endl;
				liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
			}


		cudaEventRecord( stop, 0);cudaEventSynchronize(stop);
		cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
        //cout << "\nTempo de copia: "<< tempoExecucao/1000<<"s"<< endl;
		
		//energia
		cudaEventRecord( start, 0);	

		//kernel para calcular a energia de cada pixel (executado somente uma vez)
		calculaEnergia<<<blocksPerGrid, THREADS_PER_BLOCK>>>(d_imagem, d_imagemMarcada, largura, altura);
		erro = cudaGetLastError();
		if(erro != cudaSuccess) {
            //cout << "\nCuda error81 -- %s\n"<< cudaGetErrorString(erro)<< endl;
				liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
		}

		cudaEventRecord( stop, 0);cudaEventSynchronize(stop);
		cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
        //cout << "\nTempo de energia: "<< tempoExecucao/1000<<"s"<< endl;

		if(qntRetirarLargura > 0) {
			d_imagem = redimensionaEmGPU(d_posicaoMarcado, d_imagem, d_imagemMarcada,
							largura, altura, qntRetirarLargura);
							
		erro = cudaGetLastError();
		if(erro != cudaSuccess || d_imagem == NULL) {
            //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
				liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
				free(resultado.imagemFinal);
				free(resultado.imagemMarcada);
				resultado.imagemFinal = NULL;
				resultado.imagemMarcada = NULL;
				return resultado;
		}
		
			largura -= qntRetirarLargura;
            //cout << "\nTérmino de retirada de colunas"<< endl;
		}
		
		
        if(qntRetirarAltura > 0) {        	
        
        	erro = cudaMalloc((void**)&d_imagemAux, qntDePixels * sizeof(pixelSeamCarving));
			if(erro != cudaSuccess) {
                //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
					liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
					free(resultado.imagemFinal);
					free(resultado.imagemMarcada);
					resultado.imagemFinal = NULL;
					resultado.imagemMarcada = NULL;
					return resultado;
			}

			
            inverteImagem<<<blocksPerGrid, THREADS_PER_BLOCK>>>(d_imagem, d_imagemAux, largura, altura);
			
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
					liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
					free(resultado.imagemFinal);
					free(resultado.imagemMarcada);
					resultado.imagemFinal = NULL;
					resultado.imagemMarcada = NULL;
					return resultado;
			}
            tmp = d_imagem;
            d_imagem = d_imagemAux;
            d_imagemAux = tmp;
            
            int tmpLargura = largura;
            largura = altura;
            altura = tmpLargura;
			cudaFree(d_imagemAux);
        
            //cout << "\nFim da rotação1"<< endl;

			d_imagem = redimensionaEmGPU(d_posicaoMarcado, d_imagem, d_imagemMarcada,
							largura, altura, qntRetirarAltura);

							
			erro = cudaGetLastError();
			if(erro != cudaSuccess || d_imagem == NULL) {
                //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
					liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
					free(resultado.imagemFinal);
					free(resultado.imagemMarcada);
					resultado.imagemFinal = NULL;
					resultado.imagemMarcada = NULL;
					return resultado;
			}
        	largura -= qntRetirarAltura;
        	
        	erro = cudaMalloc((void**)&d_imagemAux, qntDePixels * sizeof(pixelSeamCarving));
			if(erro != cudaSuccess ) {
                //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
					liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
					free(resultado.imagemFinal);
					free(resultado.imagemMarcada);
					resultado.imagemFinal = NULL;
					resultado.imagemMarcada = NULL;
					return resultado;
			}
        	
            inverteImagem<<<blocksPerGrid, THREADS_PER_BLOCK>>>(d_imagem, d_imagemAux, largura, altura);
			erro = cudaGetLastError();
			if(erro != cudaSuccess) {
                //cout << "\nCuda error! -- "<< cudaGetErrorString(erro)<< endl;
					liberaMemoria(d_imagem, NULL, d_posicaoMarcado, d_imagemMarcada);
					free(resultado.imagemFinal);
					free(resultado.imagemMarcada);
					resultado.imagemFinal = NULL;
					resultado.imagemMarcada = NULL;
					return resultado;
			}

            tmp = d_imagem;
            d_imagem = d_imagemAux;
            d_imagemAux = tmp;
            
            tmpLargura = largura;
            largura = altura;
            altura = tmpLargura;
            
        	cudaFree(d_imagemAux);
            //cout << "\nFim da rotação2"<< endl;
		}            

		cudaEventRecord( start, 0);

		
		//copia de volta para a memória principal a imagem marcada
		cudaMemcpy(resultado.imagemMarcada, d_imagemMarcada, sizeof(pixelRGB)*qntDePixels, cudaMemcpyDeviceToHost);
		
		
		//Copia de volta para a memória principal a imagem redimensionada
		qntDePixels = largura * altura;
		cudaMemcpy(resultado.imagemFinal, d_imagem, sizeof(pixelSeamCarving)*qntDePixels, cudaMemcpyDeviceToHost);
		
		
		
		cudaEventRecord( stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
        //cout << "\nTempo de copiar de volta para o host: "<< tempoExecucao/1000<<"s"<< endl;

		cudaEventRecord( start, 0);
		
		liberaMemoria(d_imagem, d_imagemAux, d_posicaoMarcado, d_imagemMarcada );
		
		cudaEventRecord( stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime( &tempoExecucao, start, stop ) ;
        //cout << "\nTempo de liberar memória: "<< tempoExecucao/1000<<"s"<< endl;

		cudaEventDestroy( start );
		cudaEventDestroy( stop );

		return resultado;
	}

}
