#ifndef CONSOLE_H
#define CONSOLE_H
#include <QWidget>
#include <QVBoxLayout>
#include <QTextEdit>
#include "../header/Q_DebugStream.h"

class Console:public QWidget
{
    Q_OBJECT
public:
    Console(QWidget *parent = 0);
    virtual ~Console();
    void println(char *msg);
    void clean();
    QTextEdit* getTextArea();
private:
    void createWindow();

    QVBoxLayout *mainLayout;
    QTextEdit *textArea;

};

#endif // CONSOLE_H
