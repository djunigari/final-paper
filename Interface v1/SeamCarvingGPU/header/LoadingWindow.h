#ifndef LOADINGWINDOW_H
#define LOADINGWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QMovie>

class LoadingWindow:public QWidget
{
    Q_OBJECT
public:
    LoadingWindow(QWidget *parent = 0);
    virtual ~LoadingWindow();
    QLabel* getTimeLabel();
public:

    void createWindow();
private:
    QVBoxLayout *mainLayout;
    QLabel *msg;
    QLabel *time;
    QLabel *image;
    QMovie *movie;
};
#endif // LOADINGWINDOW_H


