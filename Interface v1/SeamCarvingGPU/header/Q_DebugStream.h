#ifndef Q_DEBUGSTREAM_H
#define Q_DEBUGSTREAM_H
#include <iostream>
#include <streambuf>
#include <string>
#include <QTextEdit>

using namespace std;

class Q_DebugStream : public basic_streambuf<char>{
public:
    Q_DebugStream(ostream &stream, QTextEdit* text_edit);
    ~Q_DebugStream();
    static void registerQDebugMessageHandler();

protected:
    //This is called when a std::endl has been inserted into the stream
    virtual int_type overflow(int_type v);
    virtual streamsize xsputn(const char *p, streamsize n);

private:
    static void myQDebugMessageHandler(QtMsgType, const QMessageLogContext &, const QString &msg);

    ostream &m_stream;
    streambuf *m_old_buf;
    QTextEdit* log_window;
};

#endif // Q_DEBUGSTREAM_H
