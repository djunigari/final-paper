/*
 * VerificaGPU.h
 *
 *      Author: ar1
 */

#ifndef VERIFICAGPU_H_
#define VERIFICAGPU_H_

#include <string>

using namespace std;

typedef struct gpuQuery {
	bool existeGpu;
	int majorCapability;
	int minorCapability;
	std::string nomeGpu;
	std::string detalhesErro;
}gpuQuery;

gpuQuery getGpuQuery(int minimumCapability_major, int minimumCapability_minor);

#endif /* VERIFICAGPU_H_ */
