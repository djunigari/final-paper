#ifndef SEAMCARVINGTHREAD_H
#define SEAMCARVINGTHREAD_H
#include <QThread>
#include <QApplication>
#include "../header/SeamCarving.h"
enum TypeProcess{CPU = 0, GPU = 1};
class SeamCarvingThread:public QThread{
    Q_OBJECT
public:
    SeamCarvingThread();
    ~SeamCarvingThread();
    SeamCarvingThread* setOriginalImage(pixelSeamCarving* originalImage);
    SeamCarvingThread* setTypeProcess(TypeProcess typeProcess);
    SeamCarvingThread* setQntColunasRetirar(int qntRetirarLargura);
    SeamCarvingThread* setQntLinhasRetirar(int qntRetirarAltura);
    SeamCarvingThread* setOriginalWidth(int originalWidth);
    SeamCarvingThread* setOriginalHeight(int originalHeight);
    resultadoSeamCarving& getResultSeamCarving();
signals:
    void processCompleted();
private:
    void run();
    TypeProcess typeProcess;
    pixelSeamCarving* originalImage;
    int qntRetirarLargura;
    int qntRetirarAltura;
    int originalWidth;
    int originalHeight;
    resultadoSeamCarving result;
};
#endif // SEAMCARVINGTHREAD_H
