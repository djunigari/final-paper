#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QFile>
#include <QString>
#include <QAction>


class AboutWindow:public QWidget{
    Q_OBJECT
public:
    AboutWindow(QString filePath, QAction *action,QWidget *parent = 0);
    virtual ~AboutWindow();
    AboutWindow* setWidth(int w);
    AboutWindow* setHeight(int h);
signals:
    void sendClose(QAction *action);
private:
    void createWindow();
    int width;
    int height;
    QAction *action;
    QVBoxLayout *mainLayout;
    QTextEdit *textArea;
    QString filePath;
};

#endif // ABOUTWINDOW_H
