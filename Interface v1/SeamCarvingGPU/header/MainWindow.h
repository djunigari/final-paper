#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QList>
#include <QMainWindow>
#include <QtWidgets>
#include <QLCDNumber>
#include "../header/ClockThread.h"
#include "../header/ImageUtils.h"
#include "../header/SeamCarvingThread.h"
#include "../header/ImageWindow.h"
#include "../header/Console.h"
#include "../header/LoadingWindow.h"
#include "../header/VerificaGPU.h"

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
QT_END_NAMESPACE

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();
private slots:
    void open();
    void save();
    void saveAs();
    void openRecentFile();
    void about();
    void howTo();
    void play();
    void showFileTool();
    void showProcessTool();
    void showOriginalImageTab();
    void showResultImageTab();
    void showMarkedImageTab();
    void showConsole();
    void closeTab(int i,QAction *action);
    void setResultSeamCarving();
    void setTabSelecionado(int i);
    void closeAboutWindow(QAction *action);
private:
    void createActions();
    void createMenus();
    void createToolBars();
    void createWindow();
    void loadFile(const QString &fileName);
    void saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);
    void openImage(const QString &fileName);
    void updateImage();
    void updateRecentFileActions();

    QString strippedName(const QString &fullFileName);

    QString curFile;

    QMenu *fileMenu;
    QMenu *recentFilesMenu;
    QMenu *windowMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *processToolBar;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *aboutAct;
    QAction *howToAct;
    QAction *aboutQtAct;
    QAction *separatorAct;
    QAction *fileToolAct;
    QAction *processToolAct;
    QAction *playAct;
    QAction *originalImageAct;
    QAction *resultImageAct;
    QAction *markedImageAct;
    QAction *consoleAct;
    QSpinBox *sHeight;
    QSpinBox *sWidth;
    QGroupBox *groupBox;
    QRadioButton *radioCPU;
    QRadioButton *radioGPU;
    QLCDNumber *timeClock;
    ClockThread *clockThread;

    QSignalMapper *m_sigmapper;
    QTabWidget *mainWindow;
    ImageWindow *originalImageTab;
    ImageWindow *resultImageTab;
    ImageWindow *markedImageTab;
    Console *console;
    ImageUtils *imageUtils;

    QPixmap *originalImage;
    QPixmap *resultImage;
    QPixmap *markedImage;

    LoadingWindow *loadingWindow;
    SeamCarvingThread *seamCarvingThread;

    QString tabSelecionado;
    enum { MaxRecentFiles = 5 };
    QAction *recentFileActs[MaxRecentFiles];
    const static int widthInitial = 800;
    const static int heightInitial = 600;
};

#endif
