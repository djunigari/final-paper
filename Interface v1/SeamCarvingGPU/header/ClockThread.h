#ifndef CLOCKTHREAD_H
#define CLOCKTHREAD_H
#include <QThread>
#include <QTimer>
#include <QDateTime>
#include <QLCDNumber>

enum Status{INIT = 0,PLAY = 1,PAUSE = 2, RESET = 3,STOP = 4};
class ClockThread : public QThread{
    Q_OBJECT
public:
    ClockThread();
    ~ClockThread();
    ClockThread* setFormat(QString format);
    QString getFormat();
    QString getTime();
signals:
    void sendTime(QString time);
private slots:
    void timerHit();
    void pause();
    void play();
    void reset();
private:
    void run();
    int totalTime;
    int i;
    QString format;
    QTimer *timer;
    QDateTime *timeCount;
    Status status;
};
#endif // CLOCKTHREAD_H
