/*
 * SeamCarving.h
 *
 *      Author: ar1
 */

#ifndef SEAMCARVING_H_
#define SEAMCARVING_H_

typedef struct pixelRGB {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
}pixelRGB;

typedef struct pixelSeamCarving {
	pixelRGB pixel;
	int originalOffset;
	float energia;
	float custo;
	char deslocamento;
}pixelSeamCarving;

typedef struct resultadoSeamCarving {
	pixelSeamCarving* imagemFinal;
	pixelRGB* imagemMarcada;
}resultadoSeamCarving;


resultadoSeamCarving carvingGPU(pixelSeamCarving* imagem, int qntRetirarLargura, int qntRetirarAltura, int largura, int altura);

resultadoSeamCarving carvingCPU(pixelSeamCarving* imagem, int qntRetirarLargura, int qntRetirarAltura, int largura, int altura);

#endif /* SEAMCARVING_H_ */
