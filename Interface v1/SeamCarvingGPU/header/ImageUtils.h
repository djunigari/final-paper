#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QPixmap>
#include "../header/SeamCarving.h"

class ImageUtils{
public:
    pixelSeamCarving* convertQPixmapToPixelSeamCarving(const QPixmap &image);
    QPixmap* convertPixelSeamCarvingToQPixmap(const pixelSeamCarving *pixels, const int &width, const int &heigth);
    QPixmap* convertPixelRGBToQPixmap(const pixelRGB *pixels, const int &width, const int &heigth);
};
#endif // IMAGEUTILS_H
