#ifndef IMAGEWINDOW_H
#define IMAGEWINDOW_H

#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QToolButton>
#include <QSlider>
#include <QToolTip>

class ImageWindow:public QWidget
{
    Q_OBJECT
public:
    ImageWindow(QWidget *parent = 0);
    virtual ~ImageWindow();
    ImageWindow* setImage(QPixmap *image);
    QPixmap* getImage();
    void updateImage();
    double calcScale(double scale);
    void setScale(double scale);

private slots:
    void fitInOutView();
    void zoom(int i);

private:
    void createWindow();

    QPixmap *image;
    QVBoxLayout *mainLayout;
    QHBoxLayout *bottomLayout;
    QGraphicsView *graphicsView;
    QGraphicsScene *scene;
    QGraphicsPixmapItem *pixmapItem;
    QWidget *bottomWindow;
    QToolButton *fitButton;
    bool fitIn;
    double m_scale;
    QSlider *zoomSlider;
    QIcon iconFitIn;
    QIcon iconFitOut;
};

#endif // IMAGEWINDOW_H
