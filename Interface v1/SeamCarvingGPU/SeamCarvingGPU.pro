QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SeamCarvingGPU
TEMPLATE = app

CONFIG += static

SOURCES += src/main.cpp\
        src/MainWindow.cpp\
        src/AboutWindow.cpp\
        src/ImageWindow.cpp \
        src/ImageUtils.cpp \
        src/Console.cpp\
        src/Q_DebugStream.cpp\
        src/ClockThread.cpp\
        src/LoadingWindow.cpp \
        src/SeamCarvingThread.cpp\
        src/SeamCarvingCPU.cpp \
        src/SeamCarvingGPU.cu \
        src/VerificaGPU.cu

HEADERS  += header/MainWindow.h\
        header/AboutWindow.h\
        header/ImageWindow.h\
        header/ImageUtils.h\
        header/Console.h\
        header/Q_DebugStream.h\
        header/ClockThread.h \
        header/LoadingWindow.h\
        header/VerificaGPU.h \
        header/SeamCarvingThread.h\
        header/SeamCarving.h

RESOURCES += \
    resources/Icons.qrc \
    resources/pages.qrc \
    resources/HelpWindow.qrc

RC_FILE = resources/icon.rc

SOURCES -= src/SeamCarvingGPU.cu \
            src/VerificaGPU.cu

# Definição das pastas de output
DESTDIR = release
OBJECTS_DIR = release/obj

################# Início da Configuração de compilação CUDA ######################
# IMPORTANTE: Mudar, conforme a necessidade, SOMENTE os valores de:
# 'CUDA_DIR'    (Diretório de instalação CUDA)
# 'SYSTEM_NAME' (SOMENTE NO WINDOWS: 'Win32' ou 'x64'. Deve estar de acordo com a instalação do QT (32 ou 64 bits))
# 'SYSTEM_TYPE' (tipo do sistema: '32' ou '64'. Deve estar de acordo com a instalação do QT (32 ou 64 bits))
# Os valores dos PATHs estão definidos como o padrão de instalação CUDA
# As arquiteturas (32 ou 64) são independentes entre a compilação para windows e linux.

# Cuda sources
CUDA_SOURCES += src/SeamCarvingGPU.cu \
                src/VerificaGPU.cu

win32 {
    # CUDA settings <-- mudar se as configurações de sistema forem diferentes:
    CUDA_DIR = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v5.5"  # Path para o diretório de instalação CUDA
    SYSTEM_NAME = Win32    # Depende do sistema. Valores: 'Win32' || 'x64'
    SYSTEM_TYPE = 32   # '32' or '64', dependendo do sistema

    # definição do path para as bibliotecas
    QMAKE_LIBDIR += $$CUDA_DIR/lib/$$SYSTEM_NAME
    CUDA_LIBS = -lcudart -lcuda
    LIBS += "$$CUDA_DIR/lib/$$SYSTEM_NAME/cuda.lib" \
            "$$CUDA_DIR/lib/$$SYSTEM_NAME/cudart.lib"

}
unix {
    CUDA_DIR = "/usr/local/cuda-5.5" # Path para o diretório de instalação CUDA
    SYSTEM_TYPE = 64    #'32' or '64', dependendo do sistema
    contains(SYSTEM_TYPE, 32) {
        QMAKE_LIBDIR += $$CUDA_DIR/lib  #32 bits: $$CUDA_DIR/lib  || 64 bits: $$CUDA_DIR/lib64
    }
    else {
        QMAKE_LIBDIR += $$CUDA_DIR/lib64  #32 bits: $$CUDA_DIR/lib  || 64 bits: $$CUDA_DIR/lib64
    }
    CUDA_LIBS = -lcudart -lcuda
    LIBS += $$CUDA_LIBS
}
# include paths
INCLUDEPATH += $$CUDA_DIR/include
CUDA_ARCH = sm_20           # Tipo da arquitetura CUDA, por exemplo 'compute_11', 'compute_21', 'sm_30'
#NVCC_OPTIONS = --use_fast_math  #opções extras (não utilizadas nesse projeto)

# Definição da pasta de output dos códigos compilados
CUDA_OBJECTS_DIR = $$OBJECTS_DIR

# coloca entre aspas o PATH, que frequentemente possui espaços que causam problemas na compilação:
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')

#define o compilador para CUDA
CUDA_COMPILER = $$CUDA_DIR/bin/nvcc

cuda.input = CUDA_SOURCES
cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.obj
cuda.commands = $$CUDA_COMPILER $$NVCC_OPTIONS $$CUDA_INC $$CUDA_LIBS --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
cuda.dependency_type = TYPE_C
QMAKE_EXTRA_COMPILERS += cuda

################# Fim da Configuração de compilação CUDA ######################
